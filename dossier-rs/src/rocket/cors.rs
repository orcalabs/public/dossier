//! Implement a very basic CORS fairing.

use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::Header;
use rocket::{Request, Response};

/// Unit structure implementing CORS functionality as a rocket fairing.
#[derive(Default)]
pub struct Cors;

#[rocket::async_trait]
impl Fairing for Cors {
    fn info(&self) -> Info {
        Info {
            name: "CORS",
            kind: Kind::Response,
        }
    }

    async fn on_response<'a>(&'a self, _request: &'a Request<'_>, response: &'a mut Response<'_>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Headers",
            "Authorization, Accept",
        ));
        response.set_header(Header::new("Access-Control-Allow-Methods", "GET"));
    }
}
