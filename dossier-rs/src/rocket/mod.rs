//! Rocket ready integration to serve in your web server application.

pub mod cors;
pub mod endpoints;

use self::cors::Cors;
use crate::backend::{DataPointIndex, DataPointStore};

/// Create the rocket instance with all handlers,
/// using the provided `FieldIndex` as state.
pub fn rocket(index: DataPointIndex, store: DataPointStore) -> rocket::Rocket {
    let mut rocket = rocket::ignite();

    for (base, endpoints) in endpoints::ALL_ENDPOINTS.iter() {
        rocket = rocket.mount(base, endpoints.clone());
    }
    rocket.manage(index).manage(store).attach(Cors::default())
}
