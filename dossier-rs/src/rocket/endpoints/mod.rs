//! All endpoints exposed by the service.

use once_cell::sync::Lazy;
use rocket::Route;

use std::collections::HashMap;

pub mod compat;
pub mod v1_0;

/// Defines all the different version of the API.
/// The base (API version) is the key of the hashmap and its
/// value contains a vec of all routes within that version of the API.
pub static ALL_ENDPOINTS: Lazy<HashMap<&'static str, Vec<Route>>> = Lazy::new(|| {
    let mut routes = HashMap::new();
    routes.insert("/compat", (*compat::ENDPOINTS_COMPAT).clone());
    routes.insert("/v1.0", (*v1_0::ENDPOINTS_V1_0).clone());

    routes
});
