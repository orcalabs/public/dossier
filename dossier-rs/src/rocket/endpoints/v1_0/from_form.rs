//! All FromFrom models and implementations.

use crate::schema::generic_point::GenericPointReference;

use rocket::request::FromForm;

#[derive(FromForm)]
pub struct GenericPointReferenceWire {
    id: String,
    unit: String,
}

impl From<GenericPointReferenceWire> for GenericPointReference {
    fn from(point: GenericPointReferenceWire) -> GenericPointReference {
        GenericPointReference {
            id: point.id,
            unit: point.unit,
        }
    }
}
