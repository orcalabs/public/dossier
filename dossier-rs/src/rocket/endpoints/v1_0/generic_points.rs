//! All Endpoints for querying generic points.

use crate::backend::DataPointStore;
use crate::rocket::endpoints::v1_0::from_form::GenericPointReferenceWire;
use crate::rocket::endpoints::v1_0::response::{ErrorResponse, SuccessResponse};
use crate::schema::generic_point::{GenericPoint, GenericPointReference};
use crate::schema::source_point::SourcePoint;

use rocket::http::Status;
use rocket::request::Form;
use rocket::{get, State};

use std::collections::HashSet;

/// Get a list of all generic units.
#[get("/datapoints/generic/units")]
pub fn list_units(store: State<'_, DataPointStore>) -> SuccessResponse<HashSet<String>> {
    store.generic_point_units().into()
}

/// Get a list of all `GenericPoint`s within a unit.
#[get("/datapoints/generic/<unit>")]
pub fn generic_points_in_unit(
    unit: String,
    store: State<'_, DataPointStore>,
) -> Result<SuccessResponse<Vec<GenericPoint>>, ErrorResponse> {
    let generic_points = store
        .generic_points(&unit)
        .ok_or_else(|| ErrorResponse::error(Status::NotFound, "unit not found"))?;

    Ok(generic_points.into())
}

/// Get all [SourcePoints](SourcePoint) that refer to a [GenericPoint](GenericPoint).
#[get("/datapoints/generic/sources?<generic_point_reference..>")]
pub fn source_points_from_reference(
    generic_point_reference: Form<GenericPointReferenceWire>,
    store: State<'_, DataPointStore>,
) -> Result<SuccessResponse<Vec<SourcePoint>>, ErrorResponse> {
    let generic_point_reference: GenericPointReference =
        generic_point_reference.into_inner().into();

    let source_points = store
        .source_points_from_generic_reference(&generic_point_reference)
        .ok_or_else(|| ErrorResponse::error(Status::NotFound, "reference not found"))?;

    Ok(source_points.into())
}
