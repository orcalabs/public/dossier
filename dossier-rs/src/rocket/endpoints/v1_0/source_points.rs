//! ALl endpoints related to source points.

use crate::backend::DataPointStore;
use crate::rocket::endpoints::v1_0::response::{ErrorResponse, SuccessResponse};
use crate::schema::source_point::SourcePoint;

use rocket::http::Status;
use rocket::{get, State};

use std::collections::HashMap;

/// Get a list of all [SourcePoint::source](SourcePoints::source).
#[get("/datapoints/source/sources")]
pub fn list_source_point_sources(
    store: State<'_, DataPointStore>,
) -> Result<SuccessResponse<HashMap<String, Vec<String>>>, ErrorResponse> {
    Ok(store.source_point_sources().into())
}

/// Get a list of all [SourcePoints](SourcePoint) in a unit.
#[get("/datapoints/source?<unit>&<source>")]
pub fn query_source_points(
    unit: String,
    source: String,
    store: State<'_, DataPointStore>,
) -> Result<SuccessResponse<Vec<SourcePoint>>, ErrorResponse> {
    let source_points = store
        .source_points(&unit, &source)
        .ok_or_else(|| ErrorResponse::error(Status::NotFound, "did not find any source points"))?;

    Ok(source_points.into())
}
