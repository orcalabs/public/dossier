//! Implements API responses for the v1.0 API.

use async_trait::async_trait;
use rocket::http::Header;
use rocket::http::{ContentType, Status};
use rocket::request::Request;
use rocket::response::{self, Responder, Response};
use serde::{Deserialize, Serialize};
use std::io::Cursor;

/// Successful response.
pub struct SuccessResponse<T: serde::de::DeserializeOwned + Serialize> {
    http_status: Status,
    body: Option<T>,
}

/// Erroneous response.
pub struct ErrorResponse {
    http_status: Status,
    error: ErrorResponseWire,
}

/// Error wire response to be able to serialize the error.
#[derive(Serialize, Deserialize)]
pub struct ErrorResponseWire {
    display_error: String,
}

impl<T: serde::de::DeserializeOwned + Serialize> SuccessResponse<T> {
    /// Construct a `SuccessResponse`.
    pub fn success(http_status: Status, body: T) -> SuccessResponse<T> {
        SuccessResponse {
            http_status,
            body: Some(body),
        }
    }
}

impl ErrorResponse {
    /// Construct an `ErrorResponse`.
    pub fn error<T: std::string::ToString>(http_status: Status, error: T) -> ErrorResponse {
        ErrorResponse {
            http_status,
            error: ErrorResponseWire {
                display_error: error.to_string(),
            },
        }
    }
}

#[async_trait]
impl<'r, T> Responder<'r> for SuccessResponse<T>
where
    T: serde::de::DeserializeOwned,
    T: Serialize + Send,
{
    async fn respond_to(self, _request: &'r Request<'_>) -> response::Result<'r> {
        let serialized_body =
            serde_json::to_string(&self.body).map_err(|_| Status::InternalServerError)?;

        Response::build()
            .header(ContentType::new("application", "json"))
            .header(Header::new("Access-Control-Allow-Origin", "*"))
            .header(Header::new(
                "Access-Control-Allow-Headers",
                "Authorization, Accept",
            ))
            .header(Header::new("Access-Control-Allow-Methods", "GET"))
            .sized_body(Cursor::new(serialized_body))
            .await
            .status(self.http_status)
            .ok()
    }
}

#[async_trait]
impl<'r> Responder<'r> for ErrorResponse {
    /// Clippy complains about needless lifetimes, but we can't change it due
    /// to rocket's definition of this trait.
    #[allow(clippy::needless_lifetimes)]
    async fn respond_to(self, _: &'r Request<'_>) -> response::Result<'r> {
        let serialized_error =
            serde_json::to_string(&self.error).map_err(|_| Status::InternalServerError)?;

        Response::build()
            .header(ContentType::new("application", "json"))
            .header(Header::new("Access-Control-Allow-Origin", "*"))
            .header(Header::new(
                "Access-Control-Allow-Headers",
                "Authorization, Accept",
            ))
            .header(Header::new("Access-Control-Allow-Methods", "GET"))
            .sized_body(Cursor::new(serialized_error))
            .await
            .status(self.http_status)
            .ok()
    }
}

impl<T: serde::de::DeserializeOwned + Serialize> From<T> for SuccessResponse<T> {
    fn from(body: T) -> SuccessResponse<T> {
        SuccessResponse::success(Status::Ok, body)
    }
}
