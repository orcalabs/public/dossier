//! All V1_0 API endpoints.

use once_cell::sync::Lazy;
use rocket::{routes, Route};

mod from_form;
mod generic_points;
mod response;
mod source_points;

/// Defines all v1.0 routes.
pub static ENDPOINTS_V1_0: Lazy<Vec<Route>> = Lazy::new(|| {
    routes![
        generic_points::list_units,
        generic_points::generic_points_in_unit,
        generic_points::source_points_from_reference,
        source_points::query_source_points,
        source_points::list_source_point_sources,
    ]
});
