//! All compat API endpoints.

use once_cell::sync::Lazy;
use rocket::{routes, Route};

mod compat_model;
mod endpoints;

/// Defines all compat routes.
pub static ENDPOINTS_COMPAT: Lazy<Vec<Route>> = Lazy::new(|| {
    routes![
        endpoints::list_tables,
        endpoints::get_table,
        endpoints::query_table_and_name
    ]
});
