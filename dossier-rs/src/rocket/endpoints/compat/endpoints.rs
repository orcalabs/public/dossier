//! Available endpoints for rocket

use crate::backend::DataPointIndex;
use crate::error::Error;
use crate::rocket::endpoints::compat::compat_model::CompatField;

use rocket::{get, State};
use rocket_contrib::json::Json;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;

/// Returned from /tables?query>.
#[derive(Serialize, Deserialize)]
pub struct TableQueryResponse {
    tables: HashSet<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    error: Option<Error>,
}

/// Query a table with free-text (tablename fieldname).
#[get("/tables?<query>")]
pub fn query_table_and_name(
    query: String,
    index: State<'_, DataPointIndex>,
) -> Json<TableQueryResponse> {
    let response = match index.query_unit_and_name(&query) {
        Ok(fields) => TableQueryResponse {
            tables: fields
                .iter()
                .map(|field| field.unit().to_string())
                .collect(),
            error: None,
        },
        Err(err) => TableQueryResponse {
            tables: HashSet::new(),
            error: Some(err),
        },
    };

    Json(response)
}

/// Returned from /table/<name>.
#[derive(Serialize, Deserialize, Debug)]
pub struct GetTableResponse {
    fields: Vec<CompatField>,
    error: Option<Error>,
}

/// Get all DataPoints in a Unit.
/// A Unit was previously known as a table.
#[get("/tables/<name>")]
pub fn get_table(name: String, index: State<'_, DataPointIndex>) -> Json<GetTableResponse> {
    let response = match index.data_points_in_unit(&name) {
        Ok(fields) => GetTableResponse {
            fields: fields.into_iter().map(CompatField::from).collect(),
            error: None,
        },
        Err(err) => GetTableResponse {
            fields: vec![],
            error: Some(err),
        },
    };
    Json(response)
}

/// Get a list of all the Units in the index.
/// A Unit was previously known as a table.
#[get("/tables")]
pub fn list_tables(index: State<'_, DataPointIndex>) -> Json<Vec<String>> {
    Json(index.units())
}
