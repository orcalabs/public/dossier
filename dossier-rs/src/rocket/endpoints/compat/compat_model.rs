//! Compatibility models for the frontend.

use crate::schema::data_point::DataPoint;
use crate::schema::generic_point::GenericPoint;
use crate::schema::source_point::SourcePoint;

use serde::{Deserialize, Serialize};

/// Compat model to be able to serve the correct
/// model to the frontend.
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct CompatField {
    /// Unique field identifier.
    pub id: u64,
    /// Field display name.
    pub name: String,
    /// What the value represents
    pub description: String,
    ///
    pub field_type: String,
    /// From which datasource the field is from
    pub source: String,
    /// Which unit this field belongs to.
    pub table: String,
    /// Foreign key to another table.
    /// Encoded: TABLENAME:NAME_OF_PRIMARY_KEY.
    pub foreign_key: Option<String>,
    /// Any restriction that is set on a field.
    pub restrictions: Option<Vec<String>>,
}

impl From<DataPoint> for CompatField {
    fn from(p: DataPoint) -> CompatField {
        match p {
            DataPoint::Generic(generic) => generic.into(),
            DataPoint::Source(source) => source.into(),
        }
    }
}

impl From<GenericPoint> for CompatField {
    fn from(p: GenericPoint) -> CompatField {
        CompatField {
            id: 0,
            name: p.display_name,
            description: p.description,
            field_type: p.field_type,
            source: "".to_string(),
            table: p.unit,
            foreign_key: if let Some(composition) = p.composition {
                Some(
                    composition
                        .iter()
                        .map(|c| format!("{}:{}", c.unit, c.id))
                        .collect::<Vec<String>>()
                        .join(","),
                )
            } else {
                None
            },
            restrictions: if let Some(restrictions) = p.restrictions {
                Some(
                    restrictions
                        .into_iter()
                        .map(|r| format!("{:?}", r))
                        .collect(),
                )
            } else {
                None
            },
        }
    }
}

impl From<SourcePoint> for CompatField {
    fn from(p: SourcePoint) -> CompatField {
        CompatField {
            id: 0,
            name: p.source_name,
            description: p.description,
            field_type: p.field_type,
            source: p.source,
            table: p.unit,
            foreign_key: if let Some(generic_point) = p.generic_point {
                Some(format!("{}:{}", generic_point.unit, generic_point.id))
            } else {
                None
            },
            restrictions: if let Some(restrictions) = p.restrictions {
                Some(
                    restrictions
                        .into_iter()
                        .map(|r| format!("{:?}", r))
                        .collect(),
                )
            } else {
                None
            },
        }
    }
}
