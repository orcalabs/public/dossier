//! Provides verifying the invariants of the field documentation.

use crate::error::Error;
use crate::schema::data_point::{DataPoint, DataPointReference};

use std::collections::HashMap;

/// Verify the intergrity of a set of `DataPoint`s.
pub struct Verifier {}

impl Verifier {
    /// Verify that all fields have unqiue ids.
    pub fn verify_unique_id(&mut self, data_points: &[DataPoint]) -> Result<(), Error> {
        let mut reference_map = HashMap::new();
        let mut reference_vec = Vec::new();

        // verify that all datapoints are unique.
        for data_point in data_points {
            if reference_vec.contains(&data_point) {
                return Err(Error::DuplicateId(
                    data_point.name().to_string(),
                    data_point.id().to_string(),
                ));
            }

            reference_vec.push(data_point);

            reference_map.insert(data_point.global_id(), data_point);
        }

        // Verify all references.
        for data_point in data_points {
            match data_point {
                DataPoint::Generic(p) => {
                    if let Some(composition) = &p.composition {
                        for c in composition {
                            let id = DataPointReference::Generic(c.clone()).id();
                            if !reference_map.contains_key(&id) {
                                return Err(Error::InvalidReference(format!(
                                    "generic point (id: {:?}) with reference ({:?})",
                                    p, c
                                )));
                            }
                        }
                    }
                }
                DataPoint::Source(p) => {
                    if let Some(relation) = &p.relation {
                        let id = DataPointReference::Source(relation.clone()).id();
                        if !reference_map.contains_key(&id) {
                            return Err(Error::InvalidReference(format!(
                                "source point ({:?}) with reference ({:?})",
                                p, relation
                            )));
                        }
                    }
                    if let Some(generic_point_reference) = &p.generic_point {
                        let id = DataPointReference::Generic(generic_point_reference.clone()).id();
                        if !reference_map.contains_key(&id) {
                            return Err(Error::InvalidReference(format!(
                                "source point ({:?}), with reference ({:?})",
                                p, generic_point_reference
                            )));
                        }
                    }
                }
            }
        }

        Ok(())
    }
}

impl Default for Verifier {
    /// Construct `Verifier`.
    fn default() -> Verifier {
        Verifier {}
    }
}
