//! Defines the errors exposed and used by this crate.

use failure::Fail;
use serde::{Deserialize, Serialize};
use tantivy::query::QueryParserError;
use tantivy::TantivyError;

/// Datadoc errors.
#[derive(Debug, Fail, Serialize, Deserialize)]
pub enum Error {
    /// When two or more fields have the same id.
    #[fail(display = "data_point {} has duplicate id {}", _0, _1)]
    DuplicateId(String, String),
    /// If a query itself is invalid.
    #[fail(display = "Query invalid {}", _0)]
    InvalidQuery(String),
    /// If a data point reference is invalid.
    #[fail(display = "Reference invalid {}", _0)]
    InvalidReference(String),
    /// If a query failed.
    #[fail(display = "Query failed {}", _0)]
    QueryFailed(String),
    /// Query on unindexed field.
    #[fail(display = "Unknown Field: '{}' queried", _0)]
    UnknownIndexField(String),
    ///
    #[fail(display = "Tantivy operation failed {}", _0)]
    Tantivy(String),
}

impl From<TantivyError> for Error {
    fn from(err: tantivy::Error) -> Self {
        Error::Tantivy(err.to_string())
    }
}

impl From<QueryParserError> for Error {
    fn from(qpe: QueryParserError) -> Self {
        match qpe {
            QueryParserError::SyntaxError => Error::QueryFailed("Syntax error in query".into()),
            QueryParserError::FieldDoesNotExist(e) => Error::UnknownIndexField(e),
            QueryParserError::FieldNotIndexed(e) => {
                Error::QueryFailed(format!("Query on un-indexed field {}", e))
            }
            QueryParserError::FieldDoesNotHavePositionsIndexed(e) => {
                Error::QueryFailed(format!("Field {} does not have positions indexed", e))
            }
            QueryParserError::ExpectedInt(e) => Error::QueryFailed(e.to_string()),
            QueryParserError::ExpectedFloat(e) => Error::QueryFailed(e.to_string()),
            QueryParserError::NoDefaultFieldDeclared | QueryParserError::RangeMustNotHavePhrase => {
                Error::QueryFailed("No default field declared for query".into())
            }
            QueryParserError::AllButQueryForbidden => {
                Error::QueryFailed("Cannot have queries that only exclude documents".into())
            }
            QueryParserError::UnknownTokenizer(field, tok) => {
                Error::QueryFailed(format!("Unknown tokenizer {} for field {}", tok, field))
            }
            QueryParserError::DateFormatError(p) => Error::QueryFailed(p.to_string()),
        }
    }
}
