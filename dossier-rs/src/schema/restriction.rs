//! Holds the definition of a data point restriction.

use serde::{Deserialize, Serialize};

/// Intermediate representation of a restriction
/// to be able to construct an enum based on the toml value.
/// We must do this since enum deserialization is not supported for toml.
///
/// Eks:
/// ```no_compile
/// [restriction]
///     value = 2
///     descritpion = "desc"
/// ```
#[derive(Serialize, Deserialize, Debug)]
pub struct RestrictionDeserialize {
    value: toml::Value,
    description: Option<String>,
}

/// Represents a data point restriction.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Restriction {
    description: Option<String>,
    restriction_type: RestrictionType,
}

/// Defines the types a restriction can have.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum RestrictionType {
    /// Integer.
    Integer(i64),
    /// Float.
    Float(f64),
    /// Integer Range.
    IntegerRange(i64, i64),
    /// Float Range.
    FloatRange(f64, f64),
    /// String range.
    String(String),
}

/// Deserialize a vec of restriction.
pub fn deserialize_restriction<'de, D>(
    deserializer: D,
) -> Result<Option<Vec<Restriction>>, D::Error>
where
    D: serde::de::Deserializer<'de>,
{
    let restrictions_deserialize = Vec::<RestrictionDeserialize>::deserialize(deserializer)?;

    let mut restrictions = Vec::new();
    for restriction_deserialize in restrictions_deserialize {
        let restriction = match restriction_deserialize.value {
            toml::Value::Integer(value) => Restriction {
                restriction_type: RestrictionType::Integer(value),
                description: restriction_deserialize.description,
            },
            toml::Value::String(value) => Restriction {
                restriction_type: RestrictionType::String(value),
                description: restriction_deserialize.description,
            },
            toml::Value::Array(value) => {
                if value.len() != 2 {
                    return Err(serde::de::Error::invalid_length(
                        2,
                        &"array must have 2 elements",
                    ));
                }

                // TODO: Check that min is smaller than max.
                match (&value[0], &value[1]) {
                    (toml::Value::Integer(min), toml::Value::Integer(max)) => Restriction {
                        restriction_type: RestrictionType::IntegerRange(*min, *max),
                        description: restriction_deserialize.description,
                    },
                    (toml::Value::Float(min), toml::Value::Float(max)) => Restriction {
                        restriction_type: RestrictionType::FloatRange(*min, *max),
                        description: restriction_deserialize.description,
                    },
                    _ => return Err(serde::de::Error::custom("range must have the same type")),
                }
            }
            toml::Value::Float(value) => Restriction {
                restriction_type: RestrictionType::Float(value),
                description: restriction_deserialize.description,
            },
            _ => return Err(serde::de::Error::custom("unsupported restriction type")),
        };
        restrictions.push(restriction);
    }

    Ok(Some(restrictions))
}
