//! Defines a data point, which can repersent either
//! a [GenericPoint](GenericPoint) or a [DataPoint](DataPoint).

use crate::schema::generic_point::{GenericPoint, GenericPointReference};
use crate::schema::source_point::{SourcePoint, SourcePointReference};

use serde::{Deserialize, Serialize};
use strum_macros::{AsRefStr, EnumDiscriminants};

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

/// Represents a single data point.
#[derive(Deserialize, Serialize, Clone, Debug, EnumDiscriminants)]
#[strum_discriminants(name(DataPointType), derive(AsRefStr))]
pub enum DataPoint {
    /// Generic data point.
    Generic(GenericPoint),
    /// Source data point.
    Source(SourcePoint),
}

/// Implement shared functionality for the enum discriminants.
impl DataPoint {
    /// Get the data point's unit.
    pub fn unit(&self) -> &str {
        match self {
            DataPoint::Generic(p) => &p.unit,
            DataPoint::Source(p) => &p.unit,
        }
    }

    /// Get the data point's id.
    pub fn id(&self) -> &str {
        match self {
            DataPoint::Generic(p) => &p.id,
            DataPoint::Source(p) => &p.id,
        }
    }

    /// Get the data point's name.
    pub fn name(&self) -> &str {
        match self {
            DataPoint::Generic(p) => &p.display_name,
            DataPoint::Source(p) => &p.source_name,
        }
    }

    /// Get the global id of the data point.
    pub fn global_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        hasher.finish()
    }
}

impl From<GenericPoint> for DataPoint {
    fn from(p: GenericPoint) -> DataPoint {
        DataPoint::Generic(p)
    }
}

impl From<SourcePoint> for DataPoint {
    fn from(p: SourcePoint) -> DataPoint {
        DataPoint::Source(p)
    }
}

impl PartialEq for DataPoint {
    fn eq(&self, other: &DataPoint) -> bool {
        match (self, other) {
            (DataPoint::Generic(a), DataPoint::Generic(b)) => a == b,
            (DataPoint::Source(a), DataPoint::Source(b)) => a == b,
            _ => false,
        }
    }
}

impl PartialEq<DataPointType> for DataPoint {
    fn eq(&self, other: &DataPointType) -> bool {
        let self_type: &DataPointType = &self.into();
        self_type == other
    }
}

impl Hash for DataPoint {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            DataPoint::Generic(p) => {
                "Generic".hash(state);
                p.hash(state);
            }
            DataPoint::Source(p) => {
                "Source".hash(state);
                p.hash(state);
            }
        }
    }
}

/// Holds the difference referencetypes.
/// Its purpose is to allow generalising references to any data point.
#[derive(Clone)]
pub enum DataPointReference {
    /// Reference to a `GenericPoint`.
    Generic(GenericPointReference),
    /// Reference to a `SourcePoint`.
    Source(SourcePointReference),
}

impl Hash for DataPointReference {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            DataPointReference::Generic(p) => {
                "Generic".hash(state);
                p.hash(state);
            }
            DataPointReference::Source(p) => {
                "Source".hash(state);
                p.hash(state);
            }
        }
    }
}

impl DataPointReference {
    /// Get the id of the [DataPoint](DataPoint) that the reference points to.
    pub fn id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        hasher.finish()
    }
}
