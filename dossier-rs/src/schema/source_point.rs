//! Define the schema for `source_point` information structures.

use serde::{Deserialize, Serialize};

use crate::schema::generic_point::GenericPointReference;
use crate::schema::restriction::{deserialize_restriction, Restriction};

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

/// Represents a source data point.
///
/// Example toml:
/// ```no_compile
/// [[source_point]]
/// id = "field_id"
/// source_name = "Field name"
/// description = "This is a field"
/// field_type = "Number(1)"
/// source = "DataWarehouse"
/// unit = "unit_name"
/// relation = "SOURCE_POINT_ID:SOURCE_POINT_UNIT"
/// restrictions = ["0", "1"]
/// [generic_point]
///     id = "data_point_id"
///     unit = "data_point_unit"
/// ```
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct SourcePoint {
    /// Unique field identifier.
    pub id: String,
    /// What the data point is named at the source.
    pub source_name: String,
    /// What the value represents
    pub description: String,
    /// Value type.
    /// Can by anyhting from database type to data type.
    pub field_type: String,
    /// Which unit this source points belongs to.
    pub unit: String,
    /// Source of the data point.
    pub source: String,
    /// Relation (reference) to another source point.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub relation: Option<SourcePointReference>,
    /// Any restriction that is set on this source point.
    #[serde(
        default,
        deserialize_with = "deserialize_restriction",
        rename = "restriction",
        skip_serializing_if = "Option::is_none"
    )]
    pub restrictions: Option<Vec<Restriction>>,
    /// Ref to a [GenericPoint](../generic_point/struct.GenericPoint.html).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub generic_point: Option<GenericPointReference>,
}

/// Defines a reference to a [SourcePoint](SourcePoint).
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SourcePointReference {
    /// reference to [SourcePoint.id](SourcePoint::id).
    pub id: String,
    /// reference to [SourcePoint.unit](SourcePoint::unit).
    pub unit: String,
    /// reference to [SourcePoint.source](SourcePoint::source).
    pub source: String,
}

impl SourcePointReference {
    /// Get the hash value of the reference.
    pub fn to_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        hasher.finish()
    }
}

impl PartialEq for SourcePoint {
    fn eq(&self, other: &SourcePoint) -> bool {
        self.source == other.source && self.id == other.id && self.unit == other.unit
    }
}

impl Hash for SourcePoint {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
        self.unit.hash(state);
        self.source.hash(state);
    }
}

impl From<SourcePoint> for SourcePointReference {
    fn from(point: SourcePoint) -> SourcePointReference {
        SourcePointReference {
            id: point.id,
            unit: point.unit,
            source: point.source,
        }
    }
}

impl From<&SourcePoint> for SourcePointReference {
    fn from(point: &SourcePoint) -> SourcePointReference {
        SourcePointReference {
            id: point.id.clone(),
            unit: point.unit.clone(),
            source: point.source.clone(),
        }
    }
}
