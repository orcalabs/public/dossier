//! Define the schema for `data_point` information structures.

use serde::{Deserialize, Serialize};

use crate::schema::restriction::{deserialize_restriction, Restriction};

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

/// Represents a generic data point.
///
/// Example toml:
/// ```no_compile
/// [[generic_point]]
/// id = "field_id"
/// code_name = "field_id"
/// display_name = "Display name"
/// description = "This is a field"
/// field_type = "Number(1)"
/// unit = "unit_name"
/// restrictions = ["0", "1"]
/// [[generic_point.composition]]
///     id = "first_generic_point"
///     unit "first_generic_point_unit"
/// [[generic_point.composition]]
///     id = "second_generic_point"
///     unit "second_generic_point_unit"
/// ```
///
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct GenericPoint {
    /// Unique identifier.
    pub id: String,
    /// Formatted for code generation.
    pub code_name: String,
    /// Displayable name.
    pub display_name: String,
    /// What the value represents.
    pub description: String,
    /// Value type.
    /// Can be anyhting from database type to data type.
    pub field_type: String,
    /// A unit logically couples GenericPoints together, whether it be a domain,
    /// a database table or a collective term for a certain type of information.
    pub unit: String,
    /// A GenericPoints can be composed of other GenericPoints,
    /// a composition therefore references which GenericPoints.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub composition: Option<Vec<GenericPointReference>>,
    /// Any restriction that is set a GenericPoint.
    #[serde(
        default,
        deserialize_with = "deserialize_restriction",
        rename = "restriction",
        skip_serializing_if = "Option::is_none"
    )]
    pub restrictions: Option<Vec<Restriction>>,
}

/// Defines a reference to a [GenericPoint](GenericPoint).
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GenericPointReference {
    /// reference to [GenericPoint.id](GenericPoint::id).
    pub id: String,
    /// reference to [GenericPoint.unit](GenericPoint::unit).
    pub unit: String,
}

impl GenericPointReference {
    /// Get the hash value of the reference.
    pub fn to_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        hasher.finish()
    }
}

impl PartialEq for GenericPoint {
    fn eq(&self, other: &GenericPoint) -> bool {
        self.id == other.id && self.unit == other.unit
    }
}

impl Hash for GenericPoint {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
        self.unit.hash(state);
    }
}

impl From<GenericPoint> for GenericPointReference {
    fn from(point: GenericPoint) -> GenericPointReference {
        GenericPointReference {
            id: point.id,
            unit: point.unit,
        }
    }
}

impl From<&GenericPoint> for GenericPointReference {
    fn from(point: &GenericPoint) -> GenericPointReference {
        GenericPointReference {
            id: point.id.clone(),
            unit: point.unit.clone(),
        }
    }
}
