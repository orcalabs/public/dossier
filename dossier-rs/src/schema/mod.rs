//! Define schema for various serialized models

pub mod data_point;
pub mod generic_point;
pub mod restriction;
pub mod source_point;

// TODO: Add support for data_source
