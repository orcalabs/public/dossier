//! Deserialization of the field documentation.

use crate::schema::data_point::DataPoint;
use crate::schema::generic_point::GenericPoint;
use crate::schema::source_point::SourcePoint;

use failure::{format_err, Error};
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

/// Matches the `[[data_point]]` TOML arrays definition files.
#[derive(Deserialize, Debug, Serialize)]
pub struct DataPoints {
    /// Vec of generic points.
    pub generic_point: Option<Vec<GenericPoint>>,
    /// Vec of source points.
    pub source_point: Option<Vec<SourcePoint>>,
}

impl From<DataPoints> for Vec<DataPoint> {
    fn from(points: DataPoints) -> Vec<DataPoint> {
        let mut data_points = Vec::new();
        if let Some(generic_point) = points.generic_point {
            data_points.extend(
                generic_point
                    .into_iter()
                    .map(DataPoint::from)
                    .collect::<Vec<DataPoint>>(),
            );
        }
        if let Some(source_point) = points.source_point {
            data_points.extend(
                source_point
                    .into_iter()
                    .map(DataPoint::from)
                    .collect::<Vec<DataPoint>>(),
            );
        }

        data_points
    }
}

/// Deserialize a a buffer containing `[[data_point]]` TOML arrays.
pub fn parse_data_points_from_slice(buf: &[u8]) -> Result<Vec<DataPoint>, Error> {
    let points: DataPoints =
        toml::de::from_slice(buf).map_err(|err| format_err!("buf with error {}", err))?;

    Ok(points.into())
}

/// Deserialize a collection of input streams into `DataPoint`.
pub fn parse_data_points_from_filepaths(filepaths: &[PathBuf]) -> Result<Vec<DataPoint>, Error> {
    let mut data: Vec<DataPoint> = Vec::new();
    let mut content = String::new();

    for path in filepaths {
        let mut file = File::open(path)?;
        file.read_to_string(&mut content)?;
        let data_points: DataPoints = toml::de::from_str(&content)
            .map_err(|err| format_err!("file: {:?} with error {}", file, err))?;

        let data_points: Vec<DataPoint> = data_points.into();
        data.extend(data_points);
        content.clear();
    }

    Ok(data)
}

/// Walks the directory and opens all .toml files.
pub fn parse_data_points_from_dir(s: PathBuf) -> Result<Vec<DataPoint>, Error> {
    let mut files = vec![];
    let entries = std::fs::read_dir(s)?;
    for entry in entries {
        let entry = entry?;
        let path = entry.path();
        if !path.is_file() {
            continue;
        }

        match path.as_path().extension() {
            None => continue,
            Some(ext) => {
                if ext.to_str() != Some("toml") {
                    continue;
                }
            }
        }

        files.push(path);
    }

    parse_data_points_from_filepaths(&files)
}
