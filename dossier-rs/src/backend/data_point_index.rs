//! Implements indexing and quering the field documentation.

use crate::error::Error;
use crate::schema::data_point::DataPoint;

use std::collections::{HashMap, HashSet};

use tantivy::collector::TopDocs;
use tantivy::query::{QueryParser, TermQuery};
use tantivy::schema::{self, IndexRecordOption, SchemaBuilder};
use tantivy::{doc, DocAddress, Index, IndexReader, ReloadPolicy, Term};

/// Interface to build and query an index of `DataPoint`s.
pub struct DataPointIndex {
    /// Datadoc index.
    index: Index,
    /// Reader for the datadoc index.
    index_reader: IndexReader,
    /// Indexed id field.
    id_field: schema::Field,
    /// Indexed unitname.
    unit_field: schema::Field,
    /// free-text with unit name and point name
    unit_name_field: schema::Field,
    /// Stores all points by their id.
    points: HashMap<u64, DataPoint>,
}

impl DataPointIndex {
    /// Build schema for `DataPoint`.
    pub fn with_data_points(points: &[DataPoint]) -> Result<DataPointIndex, Error> {
        let mut schema_builder = SchemaBuilder::default();
        let id_field =
            schema_builder.add_u64_field("id", schema::INDEXED | schema::STORED | schema::FAST);
        let unit_field = schema_builder.add_text_field("unit", schema::STRING | schema::STORED);
        let unit_name_field = schema_builder.add_text_field("unit/name", schema::TEXT);

        let schema = schema_builder.build();

        let index = Index::create_in_ram(schema);
        let mut index_writer = index.writer(100_000_000)?;

        let mut stored_points: HashMap<u64, DataPoint> = HashMap::new();

        for data_point in points {
            let doc = doc!(id_field => data_point.global_id(),
                     unit_field => data_point.unit(),
                     unit_name_field => format!("{} {}", data_point.unit(), data_point.name()),
            );

            index_writer.add_document(doc);

            stored_points.insert(data_point.global_id(), data_point.clone());
        }

        index_writer.commit()?;

        let index_reader = index
            .reader_builder()
            .reload_policy(ReloadPolicy::OnCommit)
            .try_into()?;

        Ok(DataPointIndex {
            index,
            unit_field,
            unit_name_field,
            index_reader,
            id_field,
            points: stored_points,
        })
    }

    /// Query both unitname and name in the same string.
    pub fn query_unit_and_name(&self, query: &str) -> Result<Vec<DataPoint>, Error> {
        let searcher = self.index_reader.searcher();

        let parser = QueryParser::for_index(&self.index, vec![self.unit_name_field]);

        let query = parser.parse_query(&query)?;

        let docs = searcher.search(
            &query,
            &TopDocs::with_limit(300).order_by_u64_field(self.id_field),
        )?;

        let mut points = self.collect_result(&docs.as_ref())?;
        points.sort_by(|a, b| a.unit().partial_cmp(b.unit()).unwrap());

        Ok(points)
    }

    /// Get a list of all unit names.
    /// Usage: for Compat.
    pub fn units(&self) -> Vec<String> {
        let units: HashSet<String> = self
            .points
            .values()
            .map(|point| point.unit().to_string())
            .collect();

        let mut sorted_unit_names = vec![];
        sorted_unit_names.extend(units.into_iter());
        sorted_unit_names.sort();

        sorted_unit_names
    }

    /// Query all points in a unit.
    /// Usage: Compat only.
    pub fn data_points_in_unit(&self, unit: &str) -> Result<Vec<DataPoint>, Error> {
        let searcher = self.index_reader.searcher();

        let query = TermQuery::new(
            Term::from_field_text(self.unit_field, unit),
            IndexRecordOption::Basic,
        );

        let result = searcher.search(
            &query,
            &TopDocs::with_limit(200).order_by_u64_field(self.id_field),
        )?;

        Ok(self.collect_result(&result.as_ref())?)
    }

    /// Get all collections from a query.
    fn collect_result(&self, docs: &[(u64, DocAddress)]) -> Result<Vec<DataPoint>, Error> {
        let mut points = Vec::new();

        for (id, _) in docs {
            points.push(
                self.points
                    .get(&id)
                    .ok_or_else(|| Error::QueryFailed("id from result not it map".to_string()))?
                    .clone(),
            );
        }

        Ok(points)
    }
}
