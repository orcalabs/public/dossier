//! in-memory data point storage.

use crate::schema::data_point::DataPoint;
use crate::schema::generic_point::{GenericPoint, GenericPointReference};
use crate::schema::source_point::{SourcePoint, SourcePointReference};

use std::collections::{BTreeMap, HashMap, HashSet};

/// Groups source points into the tuple (source, unit).
#[derive(Hash, PartialEq, Clone, Ord, Eq, PartialOrd)]
pub struct SourcePointCollectionKey {
    /// The source point's source.
    source: String,
    /// The source point's unit.
    unit: String,
}

/// Stores all datapoints.
pub struct DataPointStore {
    /// All source points by id.
    source_points: BTreeMap<u64, SourcePoint>,
    /// Groups source points into Source and unit.
    source_points_grouped: BTreeMap<SourcePointCollectionKey, Vec<SourcePointReference>>,
    /// All generic points by id.
    generic_points: BTreeMap<u64, GenericPoint>,
    /// Stores generic points by unit.
    generic_points_grouped: BTreeMap<String, Vec<GenericPointReference>>,
    /// Reference map to keep track of which source points map to which generic point.
    generic_to_source: BTreeMap<GenericPointReference, Vec<SourcePointReference>>,
}

impl DataPointStore {
    /// Construct a `DataPointStore`.
    pub fn with_data_points(data_points: &[DataPoint]) -> DataPointStore {
        let mut source_points_grouped = BTreeMap::new();
        let mut generic_points_grouped = BTreeMap::new();
        let mut source_points = BTreeMap::new();
        let mut generic_points = BTreeMap::new();
        let mut generic_to_source = BTreeMap::new();

        let data_points = data_points.to_vec();

        data_points
            .iter()
            .filter_map(|p| {
                if let DataPoint::Source(p) = p {
                    Some(p)
                } else {
                    None
                }
            })
            .for_each(|p| {
                if let Some(generic_point) = &p.generic_point {
                    let entries = generic_to_source
                        .entry(generic_point.clone())
                        .or_insert_with(Vec::new);

                    entries.push(SourcePointReference {
                        id: p.id.clone(),
                        unit: p.unit.clone(),
                        source: p.source.clone(),
                    });
                }
            });

        data_points.into_iter().for_each(|point| match point {
            DataPoint::Source(p) => {
                let entries = source_points_grouped
                    .entry(SourcePointCollectionKey {
                        source: p.source.clone(),
                        unit: p.unit.clone(),
                    })
                    .or_insert_with(Vec::new);

                let reference = SourcePointReference {
                    id: p.id.clone(),
                    unit: p.unit.clone(),
                    source: p.source.clone(),
                };

                source_points.insert(reference.to_hash(), p);

                entries.push(reference);
            }
            DataPoint::Generic(p) => {
                let entries = generic_points_grouped
                    .entry(p.unit.clone())
                    .or_insert_with(Vec::new);

                let reference = GenericPointReference {
                    id: p.id.clone(),
                    unit: p.unit.clone(),
                };

                generic_points.insert(reference.to_hash(), p);

                entries.push(reference);
            }
        });

        DataPointStore {
            source_points,
            source_points_grouped,
            generic_points,
            generic_points_grouped,
            generic_to_source,
        }
    }

    /// Get a list of generic point units.
    pub fn generic_point_units(&self) -> HashSet<String> {
        self.generic_points_grouped.keys().cloned().collect()
    }

    /// Get a list of generic points in a unit.
    pub fn generic_points(&self, unit: &str) -> Option<Vec<GenericPoint>> {
        self.generic_points_grouped
            .get(unit)
            .map(|p: &Vec<GenericPointReference>| {
                p.iter()
                    .filter_map(|reference| self.generic_points.get(&reference.to_hash()))
                    .cloned()
                    .collect()
            })
    }

    /// Get a list of source points in a unit.
    pub fn source_points(&self, unit: &str, source: &str) -> Option<Vec<SourcePoint>> {
        let key = SourcePointCollectionKey {
            unit: unit.to_string(),
            source: source.to_string(),
        };

        self.source_points_grouped
            .get(&key)
            .map(|p: &Vec<SourcePointReference>| {
                p.iter()
                    .filter_map(|reference| self.source_points.get(&reference.to_hash()))
                    .cloned()
                    .collect()
            })
    }

    /// Retrieve all sources and the units within is source for all SourcePoints.
    pub fn source_point_sources(&self) -> HashMap<String, Vec<String>> {
        let mut sources = HashMap::new();

        self.source_points_grouped.keys().for_each(|k| {
            let entries = sources.entry(k.source.clone()).or_insert_with(Vec::new);
            entries.push(k.unit.clone());
        });

        sources
    }

    /// Get a list of the source pointsthat map to a generic point.
    pub fn source_points_from_generic_reference(
        &self,
        reference: &GenericPointReference,
    ) -> Option<Vec<SourcePoint>> {
        self.generic_to_source.get(reference).map(|references| {
            references
                .iter()
                .filter_map(|r| self.source_points.get(&r.to_hash()))
                .cloned()
                .collect()
        })
    }

    /// Query the store for a `SourcePoint` matching the `SourcePointReference`.
    pub fn source(&self, reference: &SourcePointReference) -> &SourcePoint {
        self.source_points
            .get(&reference.to_hash())
            .expect("SourcePoint not present in store")
    }

    /// Query the store for a `GenericPoint` matching the `GenericPointReference`.
    pub fn generic(&self, reference: &GenericPointReference) -> &GenericPoint {
        self.generic_points
            .get(&reference.to_hash())
            .expect("GenericPoint not present in store")
    }
}
