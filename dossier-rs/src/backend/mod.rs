//! Module for all things related to the DataPointIndex.

mod data_point_index;
mod store;

pub use data_point_index::DataPointIndex;
pub use store::DataPointStore;
