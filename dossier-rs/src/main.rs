#![feature(proc_macro_hygiene)]
#![feature(decl_macro)]

use failure::Error;
use std::path::PathBuf;
use structopt::StructOpt;

use dossier::backend::{DataPointIndex, DataPointStore};
use dossier::reader::parse_data_points_from_dir;
use dossier::rocket::rocket;
use dossier::verify::Verifier;

#[derive(Debug, StructOpt)]
#[structopt(name = "dossier-web")]
struct Opt {
    #[structopt(short = "d", parse(from_os_str))]
    dir: PathBuf,
}

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();

    let points = parse_data_points_from_dir(opt.dir)?;

    let mut verifier = Verifier::default();
    verifier.verify_unique_id(&points)?;

    let index = DataPointIndex::with_data_points(&points)?;
    let store = DataPointStore::with_data_points(&points);

    rocket(index, store).launch()?;

    Ok(())
}
