#![feature(proc_macro_hygiene)]
#![feature(decl_macro)]
#![deny(missing_docs)]
#![deny(warnings)]
#![deny(rust_2018_idioms)]

//! This crate implements the schema, verification, search and serving
//! of data documentation with relations between data points and data sources.

pub mod backend;
pub mod error;
pub mod reader;
pub mod rocket;
pub mod schema;
pub mod verify;
