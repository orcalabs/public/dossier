mod app;
mod events;
mod ui;

use crate::app::App;
use crate::events::{Config, Event, EventHandler};

use anyhow::anyhow;
use keyboard_types::Key as KeyType;
use keyboard_types::Modifiers;
use std::path::PathBuf;
use std::{error::Error, io, time::Duration};
use structopt::StructOpt;
use termion::event::Key;
use termion::raw::IntoRawMode;
use tui::backend::TermionBackend;
use tui::Terminal;

use dossier::backend::DataPointStore;
use dossier::reader::parse_data_points_from_dir;
use dossier::verify::Verifier;

#[derive(Debug, StructOpt)]
#[structopt(name = "orca-datasource-explorer")]
struct Opt {
    #[structopt(short = "d", parse(from_os_str))]
    dir: PathBuf,
}

fn main() -> Result<(), Box<dyn Error>> {
    let opt = Opt::from_args();

    let points = parse_data_points_from_dir(opt.dir)?;
    let mut verifier = Verifier::default();
    verifier
        .verify_unique_id(&points)
        .map_err(|e| anyhow!("{}", e))?;
    let store = DataPointStore::with_data_points(&points);

    // Initialize new terminal session
    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;

    // Configure event handling
    let handler = EventHandler::with_config(Config {
        tick_rate: Duration::from_millis(250),
        ..Config::default()
    });

    // Initialise our app
    let mut app = App::new(store);

    // Run it forever
    terminal.clear()?;
    loop {
        ui::draw(&mut terminal, &mut app)?;

        // Handle user input
        match handler.next()? {
            Event::Input(key) => {
                // Convert termion representation of keys into keyboard_types
                //
                // Currently, key handling support in termion is sub-par, and not all
                // combinations are supported.
                let mut modifier = Modifiers::empty();
                let key = match key {
                    Key::Ctrl(c) => {
                        modifier.insert(Modifiers::CONTROL);
                        // NOTE: termion only supports ascii characters with ctrl modifier
                        KeyType::Character(c.to_string())
                    }
                    Key::Up => KeyType::ArrowUp,
                    Key::Down => KeyType::ArrowDown,
                    Key::Right => KeyType::ArrowRight,
                    Key::Left => KeyType::ArrowLeft,
                    Key::Home => KeyType::Home,
                    Key::End => KeyType::End,
                    Key::Esc => KeyType::Escape,
                    Key::Char(c) => match c {
                        '\n' => KeyType::Enter,
                        c => KeyType::Character(c.to_string()),
                    },
                    _ => continue,
                };

                app.handle_key_input(modifier, key);
            }
            Event::Tick => {
                app.on_tick();
            }
        }

        if app.exit {
            terminal.clear()?;
            break;
        }
    }

    Ok(())
}
