//! Define focus objects and related handling

use keyboard_types::{Key, Modifiers};

pub struct FocusApplication;
pub struct FocusGenericTab;
pub struct FocusGenericUnitList;
pub struct FocusGenericPointTableSelected;
pub struct FocusGenericPointExpandedInfo;
pub struct FocusGenericPointCompositionList;
pub struct FocusGenericPointSourceList;

pub struct FocusSourceTab;
pub struct FocusSourceOriginList;
pub struct FocusSourceUnitList;
pub struct FocusSourcePointTable;
pub struct FocusSourcePointExpandedInfo;

impl FocusApplication {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::Application,
            focus: Box::new(Self),
        }
    }
}

impl FocusGenericTab {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::GenericTab,
            focus: Box::new(Self),
        }
    }
}

impl FocusGenericUnitList {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::GenericUnitList,
            focus: Box::new(Self),
        }
    }
}

impl FocusGenericPointTableSelected {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::GenericPointTable,
            focus: Box::new(Self),
        }
    }
}

impl FocusGenericPointExpandedInfo {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::GenericPointExpandedInfo,
            focus: Box::new(Self),
        }
    }
}

impl FocusSourceTab {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::SourceTab,
            focus: Box::new(Self),
        }
    }
}

impl FocusSourceOriginList {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::SourceOriginList,
            focus: Box::new(Self),
        }
    }
}

impl FocusSourceUnitList {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::SourceUnitList,
            focus: Box::new(Self),
        }
    }
}

impl FocusSourcePointTable {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::SourcePointTable,
            focus: Box::new(Self),
        }
    }
}

impl FocusSourcePointExpandedInfo {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::SourcePointExpandedInfo,
            focus: Box::new(Self),
        }
    }
}

impl FocusGenericPointCompositionList {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::GenericPointCompositionList,
            focus: Box::new(Self),
        }
    }
}

impl FocusGenericPointSourceList {
    pub fn layer() -> FocusLayer {
        FocusLayer {
            layer_type: FocusLayerType::GenericPointSourceList,
            focus: Box::new(Self),
        }
    }
}

pub enum FocusMessage {
    Exit,
    PopFocus,
    GenericUnitListDown,
    GenericUnitListUp,
    GenericUnitListSelection,
    GenericPointTableDown,
    GenericPointTableUp,
    GenericPointTableSelection,

    GenericPointSourceListDown,
    GenericPointSourceListUp,
    GenericPointSourceListSelection,
    GenericPointSourceListSwitch,

    GenericPointCompositionListDown,
    GenericPointCompositionListUp,
    GenericPointCompositionListSelection,
    GenericPointCompositionListSwitch,
    ExitFocusExpandedInfoList,

    SourceOriginListDown,
    SourceOriginListUp,
    SourceOriginListSelection,

    SourceUnitListDown,
    SourceUnitListUp,
    SourceUnitListSelection,

    SourcePointTableDown,
    SourcePointTableUp,

    MoveTabLeft,
    MoveTabRight,
}

#[derive(Clone, Copy, PartialEq)]
pub enum FocusLayerType {
    Application,
    GenericTab,
    GenericUnitList,
    GenericPointTable,
    GenericPointExpandedInfo,
    GenericPointCompositionList,
    GenericPointSourceList,

    SourceTab,
    SourceOriginList,
    SourceUnitList,
    SourcePointTable,
    SourcePointExpandedInfo,
}

pub struct FocusLayer {
    pub layer_type: FocusLayerType,
    pub focus: Box<dyn TuiFocus<Message = FocusMessage>>,
}

pub trait TuiFocus {
    type Message;

    /// Default implementation is to not handle the operation.
    fn handle_foreground_key_event(
        &self,
        _modifiers: Modifiers,
        _key: &Key,
    ) -> Option<Self::Message> {
        None
    }

    /// Default implementation is to not handle the operation.
    fn handle_background_key_event(
        &self,
        _modifiers: Modifiers,
        _key: &Key,
    ) -> Option<Self::Message> {
        None
    }
}

impl TuiFocus for FocusApplication {
    type Message = FocusMessage;

    fn handle_background_key_event(
        &self,
        modifiers: Modifiers,
        key: &Key,
    ) -> Option<Self::Message> {
        match modifiers {
            Modifiers::CONTROL => match key {
                Key::Character(c) if c == "c" => Some(FocusMessage::Exit),
                _ => None,
            },
            _ => match key {
                Key::Home => Some(FocusMessage::MoveTabLeft),
                Key::End => Some(FocusMessage::MoveTabRight),
                _ => None,
            },
        }
    }
}

impl TuiFocus for FocusGenericTab {
    type Message = FocusMessage;
}

impl TuiFocus for FocusSourceTab {
    type Message = FocusMessage;
}

impl TuiFocus for FocusGenericPointTableSelected {
    type Message = FocusMessage;

    fn handle_foreground_key_event(
        &self,
        modifiers: Modifiers,
        key: &Key,
    ) -> Option<Self::Message> {
        if modifiers.is_empty() {
            match key {
                Key::ArrowDown => Some(FocusMessage::GenericPointTableDown),
                Key::ArrowUp => Some(FocusMessage::GenericPointTableUp),
                Key::Enter => Some(FocusMessage::GenericPointTableSelection),
                Key::Escape => Some(FocusMessage::PopFocus),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl TuiFocus for FocusGenericUnitList {
    type Message = FocusMessage;

    fn handle_foreground_key_event(
        &self,
        modifiers: Modifiers,
        key: &Key,
    ) -> Option<Self::Message> {
        if modifiers.is_empty() {
            match key {
                Key::ArrowDown => Some(FocusMessage::GenericUnitListDown),
                Key::ArrowUp => Some(FocusMessage::GenericUnitListUp),
                Key::Enter => Some(FocusMessage::GenericUnitListSelection),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl TuiFocus for FocusSourceOriginList {
    type Message = FocusMessage;

    fn handle_foreground_key_event(
        &self,
        modifiers: Modifiers,
        key: &Key,
    ) -> Option<Self::Message> {
        if modifiers.is_empty() {
            match key {
                Key::ArrowDown => Some(FocusMessage::SourceOriginListDown),
                Key::ArrowUp => Some(FocusMessage::SourceOriginListUp),
                Key::Enter => Some(FocusMessage::SourceOriginListSelection),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl TuiFocus for FocusSourceUnitList {
    type Message = FocusMessage;

    fn handle_foreground_key_event(
        &self,
        modifiers: Modifiers,
        key: &Key,
    ) -> Option<Self::Message> {
        if modifiers.is_empty() {
            match key {
                Key::ArrowDown => Some(FocusMessage::SourceUnitListDown),
                Key::ArrowUp => Some(FocusMessage::SourceUnitListUp),
                Key::Enter => Some(FocusMessage::SourceUnitListSelection),
                Key::Escape => Some(FocusMessage::PopFocus),
                _ => None,
            }
        } else {
            None
        }
    }
}
impl TuiFocus for FocusGenericPointExpandedInfo {
    type Message = FocusMessage;
}

impl TuiFocus for FocusSourcePointExpandedInfo {
    type Message = FocusMessage;

    fn handle_foreground_key_event(
        &self,
        modifiers: Modifiers,
        key: &Key,
    ) -> Option<Self::Message> {
        if modifiers.is_empty() {
            match key {
                Key::Escape => Some(FocusMessage::ExitFocusExpandedInfoList),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl TuiFocus for FocusSourcePointTable {
    type Message = FocusMessage;

    fn handle_foreground_key_event(
        &self,
        modifiers: Modifiers,
        key: &Key,
    ) -> Option<Self::Message> {
        if modifiers.is_empty() {
            match key {
                Key::ArrowDown => Some(FocusMessage::SourcePointTableDown),
                Key::ArrowUp => Some(FocusMessage::SourcePointTableUp),
                Key::Escape => Some(FocusMessage::PopFocus),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl TuiFocus for FocusGenericPointCompositionList {
    type Message = FocusMessage;

    fn handle_foreground_key_event(
        &self,
        modifiers: Modifiers,
        key: &Key,
    ) -> Option<Self::Message> {
        if modifiers.is_empty() {
            match key {
                Key::ArrowDown => Some(FocusMessage::GenericPointCompositionListDown),
                Key::ArrowUp => Some(FocusMessage::GenericPointCompositionListUp),
                Key::ArrowRight => Some(FocusMessage::GenericPointCompositionListSwitch),
                Key::Enter => Some(FocusMessage::GenericPointCompositionListSelection),
                Key::Escape => Some(FocusMessage::ExitFocusExpandedInfoList),
                _ => None,
            }
        } else {
            None
        }
    }
}

impl TuiFocus for FocusGenericPointSourceList {
    type Message = FocusMessage;

    fn handle_foreground_key_event(
        &self,
        modifiers: Modifiers,
        key: &Key,
    ) -> Option<Self::Message> {
        if modifiers.is_empty() {
            match key {
                Key::ArrowDown => Some(FocusMessage::GenericPointSourceListDown),
                Key::ArrowUp => Some(FocusMessage::GenericPointSourceListUp),
                Key::ArrowLeft => Some(FocusMessage::GenericPointSourceListSwitch),
                Key::Enter => Some(FocusMessage::GenericPointSourceListSelection),
                Key::Escape => Some(FocusMessage::ExitFocusExpandedInfoList),
                _ => None,
            }
        } else {
            None
        }
    }
}
