use dossier::schema::{generic_point::GenericPointReference, source_point::SourcePointReference};
use tui::style::*;
use tui::widgets::Text;

pub struct Breadcrumbs {
    pub crumbs: Vec<DataPointBreadcrumbs>,
    pub texts: Vec<Text<'static>>,
}

#[derive(Clone)]
pub enum DataPointBreadcrumbs {
    Generic(GenericPointReference),
    Source(SourcePointReference),
}

impl DataPointBreadcrumbs {
    pub fn unwrap_generic(&self) -> &GenericPointReference {
        match self {
            Self::Generic(d) => &d,
            _ => panic!("expected Generic variant"),
        }
    }
}

impl Breadcrumbs {
    pub fn new() -> Breadcrumbs {
        Breadcrumbs {
            crumbs: Vec::new(),
            texts: Vec::new(),
        }
    }

    pub fn unwrap_last_crumb_generic(&self) -> &GenericPointReference {
        self.crumbs
            .last()
            .expect("missing breadcrumb")
            .unwrap_generic()
    }

    pub fn pop(&mut self) -> Option<DataPointBreadcrumbs> {
        let popped_crumb = self.crumbs.pop();
        popped_crumb.as_ref()?;

        // The last crumb text
        self.texts.pop();

        // If this returns some, we have removed a separator.
        // Modify the next text in the stack to be styled.
        if self.texts.pop().is_some() {
            let last = self.texts.pop().expect("inconsistent breadcrumbs state");
            let text = match last {
                Text::Raw(c) => c.into_owned(),
                _ => panic!("logic error - last element on breadcrumbs text stack not styled"),
            };
            self.texts
                .push(Text::styled(text, Style::default().fg(Color::Yellow)));
        }

        popped_crumb
    }

    pub fn push(&mut self, point: DataPointBreadcrumbs) {
        // Modify the existing breadcrumb text to remove the stylized text, if it exists.
        if self.crumbs.last().is_some() {
            let last = self.texts.pop().expect("inconsistent breadcrumbs state");
            // We know the last element on the text stack is styled
            let text = match last {
                Text::Styled(c, _) => c.into_owned(),
                _ => panic!("logic error - last element on breadcrumbs text stack not styled"),
            };
            self.texts.push(Text::raw(text));
        }

        let (breadcrumb_leading_text, id) = match &point {
            DataPointBreadcrumbs::Generic(g) => ("g", g.id.clone()),
            DataPointBreadcrumbs::Source(s) => ("s", s.id.clone()),
        };

        self.crumbs.push(point);

        // If this is the first crumb we add, do not add separator between texts
        if self.crumbs.len() > 1 {
            self.texts
                .push(Text::styled(" >> ", Style::default().fg(Color::DarkGray)));
        }

        // Add breadcrumbs text as the select part
        self.texts.push(Text::styled(
            format!("[{}] {}", breadcrumb_leading_text, id),
            Style::default().fg(Color::Yellow),
        ));
    }

    pub fn last(&self) -> Option<&DataPointBreadcrumbs> {
        self.crumbs.last()
    }
}
