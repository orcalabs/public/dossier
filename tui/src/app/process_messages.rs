use super::breadcrumbs::DataPointBreadcrumbs;
use super::focus::*;
use super::App;

use dossier::schema::generic_point::GenericPointReference;
use dossier::schema::source_point::SourcePointReference;
use keyboard_types::{Key, Modifiers};

impl App {
    /// Handle keystroke events that may interact with the various
    /// focus elements within the tui application.
    pub fn handle_key_input(&mut self, modifier: Modifiers, key: Key) {
        // Iterate focus stach and
        let mut iter = self.focus.iter();

        // Check if the foreground focus object can handle the event
        let event = match iter.next_back() {
            Some(ref f) => {
                match f.focus.handle_foreground_key_event(modifier, &key) {
                    Some(e) => Some(e),
                    None => {
                        // TODO: run background handling
                        f.focus.handle_background_key_event(modifier, &key)
                    }
                }
            }
            _ => None,
        };

        let event = match event {
            Some(e) => Some(e),
            None => {
                // Break loop with value
                loop {
                    match iter.next_back() {
                        Some(ref f) => {
                            let e = f.focus.handle_background_key_event(modifier, &key);
                            if let Some(e) = e {
                                break Some(e);
                            }
                        }
                        None => break None,
                    }
                }
            }
        };

        // Process the handle
        if let Some(e) = event {
            self.handle_focus_message(e);
        }
    }

    /// Individual focus messages are handled that may mutate application state.
    pub fn handle_focus_message(&mut self, message: FocusMessage) {
        match message {
            FocusMessage::GenericUnitListUp => self.on_generic_unit_list_up(),
            FocusMessage::GenericUnitListDown => self.on_generic_unit_list_down(),
            FocusMessage::GenericUnitListSelection => self.on_generic_unit_list_selection(),

            FocusMessage::GenericPointTableUp => self.on_generic_point_table_up(),
            FocusMessage::GenericPointTableDown => self.on_generic_point_table_down(),
            FocusMessage::GenericPointTableSelection => self.on_generic_point_table_selection(),

            FocusMessage::GenericPointSourceListUp => self.on_generic_expanded_source_list_up(),
            FocusMessage::GenericPointSourceListDown => self.on_generic_expanded_source_list_down(),
            FocusMessage::GenericPointSourceListSelection => {
                self.on_generic_expanded_source_list_selection()
            }
            FocusMessage::GenericPointSourceListSwitch => self.on_source_list_switch(),

            FocusMessage::GenericPointCompositionListUp => {
                self.on_generic_expanded_composition_list_up()
            }
            FocusMessage::GenericPointCompositionListDown => {
                self.on_generic_expanded_composition_list_down()
            }
            FocusMessage::GenericPointCompositionListSelection => {
                self.on_generic_expanded_composition_selection()
            }
            FocusMessage::GenericPointCompositionListSwitch => self.on_composition_list_switch(),
            FocusMessage::ExitFocusExpandedInfoList => self.on_exit_focus_expanded_info_list(),

            FocusMessage::SourceUnitListUp => self.on_source_unit_list_up(),
            FocusMessage::SourceUnitListDown => self.on_source_unit_list_down(),
            FocusMessage::SourceUnitListSelection => self.on_source_unit_list_selection(),

            FocusMessage::SourceOriginListUp => self.on_source_origin_list_up(),
            FocusMessage::SourceOriginListDown => self.on_source_origin_list_down(),
            FocusMessage::SourceOriginListSelection => self.on_source_origin_list_selection(),

            FocusMessage::SourcePointTableUp => self.on_source_point_table_up(),
            FocusMessage::SourcePointTableDown => self.on_source_point_table_down(),

            FocusMessage::MoveTabLeft => self.on_move_tab_left(),
            FocusMessage::MoveTabRight => self.on_move_tab_right(),

            FocusMessage::Exit => self.exit = true,
            FocusMessage::PopFocus => {
                if self.focus.len() > 1 {
                    self.focus.pop();
                }
            }
        }
    }

    /// Tab
    fn on_move_tab_left(&mut self) {
        let old = self.root_state.tab_index;
        self.root_state.tab_index = match self.root_state.tab_index {
            i if i != 0 => i - 1,
            i => i,
        };
        self.replace_focus_stack(old, self.root_state.tab_index);
    }

    /// Tab
    fn on_move_tab_right(&mut self) {
        let old = self.root_state.tab_index;
        self.root_state.tab_index = match self.root_state.tab_index {
            i if i < (self.root_state.tabs.len() - 1) => i + 1,
            i => i,
        };
        self.replace_focus_stack(old, self.root_state.tab_index);
    }

    /// Generic Tab Unit
    fn on_generic_unit_list_up(&mut self) {
        let i = match self.root_state.generic_tab.unit_selected.selected() {
            Some(i) if i != 0 => i - 1,
            Some(i) => i,
            None => return,
        };
        self.select_generic_unit(i);
    }

    /// Generic Tab Unit
    fn on_generic_unit_list_down(&mut self) {
        let i = match self.root_state.generic_tab.unit_selected.selected() {
            Some(i) if i < (self.root_state.generic_units.len() - 1) => i + 1,
            Some(i) => i,
            None => return,
        };

        self.select_generic_unit(i);
    }

    /// Generic Tab Unit
    fn on_generic_unit_list_selection(&mut self) {
        if let Some(i) = self.root_state.generic_tab.unit_selected.selected() {
            // We can enter the table
            self.focus.push(FocusGenericPointTableSelected::layer());

            // We should now select the first generic point
            self.select_generic_point(self.root_state.generic_units[i].clone(), 0);
        }
    }

    /// Generic Tab Point
    fn on_generic_point_table_up(&mut self) {
        let unit = match self.root_state.generic_tab.unit_selected.selected() {
            Some(i) => self.root_state.generic_units[i].clone(),
            // Unreachable - a bug has occurred
            None => unreachable!(),
        };

        let i = match self.root_state.generic_tab.point_selected.selected() {
            Some(i) if i != 0 => i - 1,
            Some(i) => i,
            None => return,
        };
        self.select_generic_point(unit, i);
    }

    /// Generic Tab Point
    fn on_generic_point_table_down(&mut self) {
        let unit = match self.root_state.generic_tab.unit_selected.selected() {
            Some(i) => self.root_state.generic_units[i].clone(),
            // Unreachable - a bug has occurred
            None => unreachable!(),
        };

        let i = match self.root_state.generic_tab.point_selected.selected() {
            Some(i) if i < (self.root_state.generic_points[&unit].len() - 1) => i + 1,
            Some(i) => i,
            None => return,
        };
        self.select_generic_point(unit, i);
    }

    /// Generic Tab Point
    fn on_generic_point_table_selection(&mut self) {
        if let Some(point_index) = self.root_state.generic_tab.point_selected.selected() {
            // We can enter the table
            self.focus.push(FocusGenericPointExpandedInfo::layer());

            let unit = self
                .root_state
                .generic_tab
                .unit_selected
                .selected()
                .unwrap();
            let unit = self.root_state.generic_units[unit].clone();
            let point = &self.root_state.generic_points[&unit][point_index];

            self.root_state
                .generic_tab
                .breadcrumbs
                .push(DataPointBreadcrumbs::Generic(point.into()));

            // Enter the Composition list
            self.enter_composition_list();
        }
    }

    /// Generic Expanded Composition
    fn on_generic_expanded_composition_list_up(&mut self) {
        // Get our current generic crumb, to validate the composition operation
        let crumb = self
            .root_state
            .generic_tab
            .breadcrumbs
            .unwrap_last_crumb_generic();
        let point = &self.store.generic(crumb);

        // Set selected entry if it is not empty
        let composition = match &point.composition {
            Some(c) => c,
            None => return,
        };
        if composition.is_empty() {
            return;
        }

        let i = match self.root_state.generic_tab.composition_selected.selected() {
            Some(i) if i != 0 => i - 1,
            Some(i) => i,
            None => return,
        };

        self.root_state
            .generic_tab
            .composition_selected
            .select(Some(i));
    }

    /// Generic Expanded Composition
    fn on_generic_expanded_composition_list_down(&mut self) {
        // Get our current generic crumb, to validate the composition operation
        let crumb = self
            .root_state
            .generic_tab
            .breadcrumbs
            .unwrap_last_crumb_generic();
        let point = &self.store.generic(crumb);

        // Set selected entry if it is not empty
        let composition = match &point.composition {
            Some(c) => c,
            None => return,
        };
        if composition.is_empty() {
            return;
        }

        let i = match self.root_state.generic_tab.composition_selected.selected() {
            Some(i) if i < (composition.len() - 1) => i + 1,
            Some(i) => i,
            None => return,
        };

        self.root_state
            .generic_tab
            .composition_selected
            .select(Some(i));
    }

    /// Generic Expanded Composition
    fn on_generic_expanded_composition_selection(&mut self) {
        // If we have no selection, we have no list.
        let composition_index = match self.root_state.generic_tab.composition_selected.selected() {
            Some(c) => c,
            _ => return,
        };
        // Make sure that the composition exists.
        let current_ref = &self
            .root_state
            .generic_tab
            .breadcrumbs
            .unwrap_last_crumb_generic();
        let composition = match &self.store.generic(current_ref).composition {
            Some(c) => c,
            None => return,
        };
        let new_ref: &GenericPointReference = &composition[composition_index];

        self.root_state
            .generic_tab
            .breadcrumbs
            .push(DataPointBreadcrumbs::Generic(new_ref.clone()));

        self.enter_composition_list();
    }

    /// Generic Expanded Source
    fn on_generic_expanded_source_list_up(&mut self) {
        // Get our current generic crumb, to validate the composition operation
        let crumb = self
            .root_state
            .generic_tab
            .breadcrumbs
            .unwrap_last_crumb_generic();

        // Set selected entry if it is not empty
        let sources = match self.store.source_points_from_generic_reference(crumb) {
            Some(c) => c,
            None => return,
        };
        if sources.is_empty() {
            return;
        }

        let i = match self.root_state.generic_tab.source_selected.selected() {
            Some(i) if i != 0 => i - 1,
            Some(i) => i,
            None => return,
        };

        self.root_state.generic_tab.source_selected.select(Some(i));
    }

    /// Generic Expanded Source
    fn on_generic_expanded_source_list_down(&mut self) {
        // Get our current generic crumb, to validate the composition operation
        let crumb = self
            .root_state
            .generic_tab
            .breadcrumbs
            .unwrap_last_crumb_generic();

        // Set selected entry if it is not empty
        let sources = match self.store.source_points_from_generic_reference(crumb) {
            Some(c) => c,
            None => return,
        };
        if sources.is_empty() {
            return;
        }

        let i = match self.root_state.generic_tab.source_selected.selected() {
            Some(i) if i < (sources.len() - 1) => i + 1,
            Some(i) => i,
            None => return,
        };

        self.root_state.generic_tab.source_selected.select(Some(i));
    }

    /// Generic Expanded Source
    fn on_generic_expanded_source_list_selection(&mut self) {
        // Guard against empty source list
        let source_index = match self.root_state.generic_tab.source_selected.selected() {
            Some(c) => c,
            _ => return,
        };

        // Get a reference to the selected source point
        let current_ref = &self
            .root_state
            .generic_tab
            .breadcrumbs
            .unwrap_last_crumb_generic();
        let source_ref: SourcePointReference =
            match self.store.source_points_from_generic_reference(current_ref) {
                Some(sources) => (&sources[source_index]).into(),
                None => return,
            };

        self.root_state
            .generic_tab
            .breadcrumbs
            .push(DataPointBreadcrumbs::Source(source_ref));

        self.enter_expanded_source_point_info();
    }

    /// Source Tab Origin
    fn on_source_origin_list_up(&mut self) {
        let i = match self.root_state.source_tab.origin_selected.selected() {
            Some(i) if i != 0 => i - 1,
            Some(i) => i,
            None => return,
        };

        self.select_source_origin(i);
    }

    /// Source Tab Origin
    fn on_source_origin_list_down(&mut self) {
        let i = match self.root_state.source_tab.origin_selected.selected() {
            Some(i) if i < (self.root_state.source_origin_units.len() - 1) => i + 1,
            Some(i) => i,
            None => return,
        };

        self.select_source_origin(i);
    }

    /// Source Tab Origin
    fn on_source_origin_list_selection(&mut self) {
        if let Some(i) = self.root_state.source_tab.origin_selected.selected() {
            // We can enter the table
            self.focus.push(FocusSourceUnitList::layer());

            // Select source unit
            self.select_source_unit(i, 0);
        }
    }

    /// Source Tab Unit
    fn on_source_unit_list_up(&mut self) {
        let origin_index = self
            .root_state
            .source_tab
            .origin_selected
            .selected()
            .expect("programmer error: origin selection unknown");

        let i = match self.root_state.source_tab.unit_selected.selected() {
            Some(i) if i != 0 => i - 1,
            Some(i) => i,
            None => return,
        };

        self.select_source_unit(origin_index, i);
    }

    /// Source Tab Unit
    fn on_source_unit_list_down(&mut self) {
        let origin_index = self
            .root_state
            .source_tab
            .origin_selected
            .selected()
            .expect("programmer error: origin selection unknown");

        let i = match self.root_state.source_tab.unit_selected.selected() {
            Some(i) if i < (self.root_state.source_origin_units[origin_index].1.len() - 1) => i + 1,
            Some(i) => i,
            None => 0,
        };

        self.select_source_unit(origin_index, i);
    }

    /// Source Tab Unit
    fn on_source_unit_list_selection(&mut self) {
        if let Some(i) = self.root_state.source_tab.unit_selected.selected() {
            // We can enter the table
            self.focus.push(FocusSourcePointTable::layer());

            // We should now select the first generic point
            let origin_index = self
                .root_state
                .source_tab
                .origin_selected
                .selected()
                .expect("inconsistent_state");
            self.select_source_point(
                self.root_state.source_origin_units[origin_index].0.clone(),
                self.root_state.source_origin_units[origin_index].1[i].clone(),
                0,
            );
        }
    }

    /// Source Tab Point
    fn on_source_point_table_up(&mut self) {
        let (origin, origin_index) = match self.root_state.source_tab.origin_selected.selected() {
            Some(i) => (self.root_state.source_origin_units[i].0.clone(), i),
            // Unreachable - a bug has occurred
            None => unreachable!(),
        };

        let unit = match self.root_state.source_tab.unit_selected.selected() {
            Some(i) => self.root_state.source_origin_units[origin_index].1[i].clone(),
            // Unreachable - a bug has occurred
            None => unreachable!(),
        };

        let i = match self.root_state.source_tab.point_selected.selected() {
            Some(i) if i != 0 => i - 1,
            Some(i) => i,
            None => return,
        };
        self.select_source_point(origin, unit, i);
    }

    /// Source Tab Point
    fn on_source_point_table_down(&mut self) {
        let (origin, origin_index) = match self.root_state.source_tab.origin_selected.selected() {
            Some(i) => (self.root_state.source_origin_units[i].0.clone(), i),
            // Unreachable - a bug has occurred
            None => unreachable!(),
        };

        let unit = match self.root_state.source_tab.unit_selected.selected() {
            Some(i) => self.root_state.source_origin_units[origin_index].1[i].clone(),
            // Unreachable - a bug has occurred
            None => unreachable!(),
        };

        let key = (origin, unit);
        let i = match self.root_state.source_tab.point_selected.selected() {
            Some(i) if i < (self.root_state.source_points[&key].len() - 1) => i + 1,
            Some(i) => i,
            None => return,
        };
        self.select_source_point(key.0, key.1, i);
    }

    /// Expanded Exit
    fn on_exit_focus_expanded_info_list(&mut self) {
        // Unwrap the last popped crumb - this handler wouldnt be present if there were none.
        let popped_crumb = self.root_state.generic_tab.breadcrumbs.pop().unwrap();
        self.focus.pop();

        // If the current breadcrumbs where empty, we also remove the top expanded info focus.
        match self.root_state.generic_tab.breadcrumbs.last() {
            Some(currentref) => {
                // Restore the selection on the currentref data point lists
                // Depending on what page we are are going to, there will be different
                // lists to select.
                match currentref {
                    DataPointBreadcrumbs::Generic(current) => {
                        // TODO: Embed last index/selection point into crumb instead of searching?
                        //
                        // The next view to render is generic point
                        // Find popped_crumb reference in either Composition or Source list
                        match popped_crumb {
                            DataPointBreadcrumbs::Generic(last) => {
                                // Locate the index of last into current
                                let current_point = &self.store.generic(current);
                                let index = current_point
                                    .composition
                                    .as_ref()
                                    .unwrap()
                                    .iter()
                                    .position(|x| x == &last)
                                    .unwrap();
                                self.root_state
                                    .generic_tab
                                    .composition_selected
                                    .select(Some(index));
                            }
                            DataPointBreadcrumbs::Source(_) => {
                                // not implemented
                            }
                        }
                    }
                    DataPointBreadcrumbs::Source(_) => {
                        // not implemented
                    }
                }
            }
            // No more items left - we are exiting the expanded view and must pop that focus
            None => {
                self.focus.pop();
            }
        }
    }

    /// Switch focus from Composition List to Source List
    fn on_composition_list_switch(&mut self) {
        // Replace the last item on the focus stack, and deselect the current composition selection.
        self.focus.pop();
        self.root_state
            .generic_tab
            .composition_selected
            .select(None);
        self.enter_source_list();
    }

    /// Switch focus from Source List to Composition List
    fn on_source_list_switch(&mut self) {
        // Replace the last item on the focus stack, and deselect the current composition selection.
        self.focus.pop();
        self.root_state.generic_tab.source_selected.select(None);
        self.enter_composition_list();
    }
}
