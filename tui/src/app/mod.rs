//! The tui application logic.

pub mod breadcrumbs;
pub mod focus;
mod process_messages;

use self::breadcrumbs::Breadcrumbs;
use self::focus::*;

use dossier::{
    backend::DataPointStore,
    schema::{generic_point::GenericPoint, source_point::SourcePoint},
};
use std::collections::hash_map::HashMap;
use tui::widgets::{ListState, TableState, Text};

/// The generic points tab renders information about GenericPoints.
///
/// It is split in two views:
/// * List on the left hand side of units.
/// * Points within selected unit/expanded points info.
pub struct GenericPointsTab {
    // Index of selection?
    pub unit_selected: ListState,
    pub point_selected: TableState,
    pub composition_selected: ListState,
    pub source_selected: ListState,
    // Poplated with the current data point selection when entered into expanded info view.
    pub breadcrumbs: Breadcrumbs,
    pub points_placeholder_text: Vec<Text<'static>>,
}

/// The source points tab renders information about SourcePoints.
///
/// It is split in two views:
/// * List on the left hand side of units.
/// * Points within selected unit/expanded points info.
pub struct SourcePointsTab {
    pub origin_selected: ListState,
    pub unit_selected: ListState,
    pub point_selected: TableState,
}

/// State object used to store all data and interactions in the tui.
pub struct RootState {
    /// State list of source origin/unit for rendering purposes.
    /// Tuple of (origin, [units])
    pub source_origin_units: Vec<(String, Vec<String>)>,
    /// Lazily populated state list of Source Points for rendering purposes.
    /// Key is tuple (origin, unit)
    pub source_points: HashMap<(String, String), Vec<SourcePoint>>,
    /// State list of generic units for rendering purposes.
    pub generic_units: Vec<String>,
    /// Lazily populated state list of Generic Points for rendering purposes.
    /// Key is unit
    pub generic_points: HashMap<String, Vec<GenericPoint>>,

    /// State for which tab is currently selected.
    pub tab_index: usize,
    /// The title definitions of our application tabs.
    pub tabs: [&'static str; 2],
    /// Whenever a tab is NOT in focus, their stack of focus will be persisted here.
    /// The tab_index determines the location.
    /// When the tab is switched in, it is replaced with an emty vec.
    pub focus_tab_history: [Vec<FocusLayer>; 2],
    /// State related to GenericTab.
    pub generic_tab: GenericPointsTab,
    /// State related to SourceTab.
    pub source_tab: SourcePointsTab,
}

/// Main application object to drive an interactive terminal ui.
pub struct App {
    pub exit: bool,

    pub root_state: RootState,

    pub store: DataPointStore,

    // Focus stack
    pub focus: Vec<FocusLayer>,
}

impl App {
    /// Create a new instance of the application.
    /// `port` argument is for the embedded rocket http server serving the data objects.
    pub fn new(store: DataPointStore) -> App {
        let mut app = App {
            exit: false,
            focus: vec![],
            store,
            root_state: RootState {
                source_origin_units: Vec::new(),
                source_points: HashMap::new(),
                generic_units: Vec::new(),
                generic_tab: GenericPointsTab {
                    unit_selected: ListState::default(),
                    point_selected: TableState::default(),
                    composition_selected: ListState::default(),
                    source_selected: ListState::default(),
                    points_placeholder_text: vec![
                        Text::raw("No selected generic unit\n"),
                        Text::raw("Please select a unit with the <Arrow up> or <Arrow down> keys, then press <Enter>"),
                    ],
                    breadcrumbs: Breadcrumbs::new(),
                },
                source_tab: SourcePointsTab {
                    origin_selected: ListState::default(),
                    unit_selected: ListState::default(),
                    point_selected: TableState::default(),
                },
                generic_points: HashMap::new(),
                tab_index: 0,
                tabs: ["Generic Points", "Source Points"],
                focus_tab_history: [
                    // Generic Tab
                    vec![FocusApplication::layer(), FocusGenericTab::layer(), FocusGenericUnitList::layer()],
                    // Source Tab
                    vec![FocusApplication::layer(), FocusSourceTab::layer(), FocusSourceOriginList::layer()],
                ],
            },
        };

        // Initialize focus stack
        app.replace_focus_stack(0, 0);

        // Initialize generic units list
        app.root_state.generic_units = app.store.generic_point_units().into_iter().collect();
        if !app.root_state.generic_units.is_empty() {
            // Chose the first element in the list
            app.select_generic_unit(0);
        }

        // Initialize source origin/units
        app.root_state.source_origin_units = app
            .store
            .source_point_sources()
            .into_iter()
            .map(|(k, v)| (k, v))
            .collect();
        if !app.root_state.source_origin_units.is_empty() {
            // Chose the first element in the list
            app.select_source_origin(0);
        }

        app
    }

    /// We support maintaining a different focus stack for each individual tab,
    /// meaning that the navigational state between each tab is maintained when switching
    /// between them.
    /// This is done by replacing the whole focus_tab_history on the tab indices we switch between,
    fn replace_focus_stack(&mut self, old: usize, new: usize) {
        // Special case if we replace the focus stack with ourselves
        if old == new {
            // If focus is empty, we are in bootup and must handle it. Otherwise, return.
            if !self.focus.is_empty() {
                return;
            }

            // This scenario handles bootup where the focus is empty
            let new_history =
                std::mem::replace(&mut self.root_state.focus_tab_history[new], vec![]);
            let _ = std::mem::replace(&mut self.focus, new_history);
            return;
        }

        // Retrieve new history
        let new_history = std::mem::replace(&mut self.root_state.focus_tab_history[new], vec![]);
        // Place new history into focus
        let old_focus = std::mem::replace(&mut self.focus, new_history);
        // store old focus
        let _ = std::mem::replace(&mut self.root_state.focus_tab_history[old], old_focus);
    }

    /// Progressive state handler. Invoked on a regualar interval.
    pub fn on_tick(&mut self) {}

    /// While navigating in the generic tab, this method fill ensure that
    /// the correct selection state are present when changing generic unit selection.
    fn select_generic_unit(&mut self, index: usize) {
        self.root_state
            .generic_tab
            .unit_selected
            .select(Some(index));
        self.root_state.generic_tab.point_selected.select(None);

        // Lazily populate the generic points for this unit
        let unit = &self.root_state.generic_units[index];
        if self.root_state.generic_points.contains_key(unit) {
            return;
        }

        if let Some(generic_points) = self.store.generic_points(unit) {
            self.root_state
                .generic_points
                .insert(unit.clone(), generic_points);
        }
    }

    fn select_generic_point(&mut self, unit: String, index: usize) {
        // Ensure that there are actually points loaded for this unit
        if !self.root_state.generic_points.contains_key(&unit) {
            return;
        }

        match self.root_state.generic_points[&unit].len() {
            0 => {
                // Unselect - It does not exist
                self.root_state.generic_tab.point_selected.select(None);
            }
            _ => {
                // Set the selection point
                self.root_state
                    .generic_tab
                    .point_selected
                    .select(Some(index));
            }
        }
    }

    /// Manipulate the source origin list selection.
    fn select_source_origin(&mut self, index: usize) {
        self.root_state
            .source_tab
            .origin_selected
            .select(Some(index));
        self.select_source_unit(index, 0);
    }

    /// Manipulate the source unit list selection.
    ///
    /// Guarded against empty unit list. Origin entry is assumed to exist.
    fn select_source_unit(&mut self, origin_index: usize, unit_index: usize) {
        if self.root_state.source_origin_units[origin_index]
            .1
            .is_empty()
        {
            return;
        }
        self.root_state
            .source_tab
            .unit_selected
            .select(Some(unit_index));

        let origin = self.root_state.source_origin_units[origin_index].0.clone();
        let unit = self.root_state.source_origin_units[origin_index].1[unit_index].clone();

        // Lazily check whjether we need to populate the point list
        let key = (origin.clone(), unit.clone());
        if self.root_state.source_points.contains_key(&key) {
            return;
        }

        if let Some(points) = self.store.source_points(&unit, &origin) {
            self.root_state.source_points.insert(key, points);
        }
    }

    fn select_source_point(&mut self, origin: String, unit: String, index: usize) {
        // Ensure that there are actually points loaded for this unit
        let key = (origin, unit);
        if !self.root_state.source_points.contains_key(&key) {
            return;
        }

        match self.root_state.source_points[&key].len() {
            0 => self.root_state.source_tab.point_selected.select(None),
            _ => self
                .root_state
                .source_tab
                .point_selected
                .select(Some(index)),
        }
    }

    fn enter_composition_list(&mut self) {
        self.focus.push(FocusGenericPointCompositionList::layer());
        // Update selection based on last breadcrumbs item
        // Since we are entering a composition list, the crumb must be generic
        let crumb = self
            .root_state
            .generic_tab
            .breadcrumbs
            .unwrap_last_crumb_generic();
        let point = &self.store.generic(crumb);

        // Set selected entry if it is not empty
        self.root_state
            .generic_tab
            .composition_selected
            .select(None);
        if let Some(c) = &point.composition {
            if !c.is_empty() {
                self.root_state
                    .generic_tab
                    .composition_selected
                    .select(Some(0));
            }
        }
    }

    fn enter_source_list(&mut self) {
        self.focus.push(FocusGenericPointSourceList::layer());
        // Update selection based on last breadcrumbs item
        // Since we are entering a composition list, the crumb must be generic
        let crumb = self
            .root_state
            .generic_tab
            .breadcrumbs
            .unwrap_last_crumb_generic();

        // Set selected entry if it is not empty
        if let Some(c) = &self.store.source_points_from_generic_reference(crumb) {
            if !c.is_empty() {
                self.root_state.generic_tab.source_selected.select(Some(0));
            }
        }
    }

    fn enter_expanded_source_point_info(&mut self) {
        self.focus.push(FocusSourcePointExpandedInfo::layer());
    }
}
