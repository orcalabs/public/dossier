//! Create and render souce components.

use super::focus_border_fg;
use crate::app::focus::FocusLayerType;
use crate::app::App;

use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout, Rect};
use tui::style::{Color, Style};
use tui::widgets::{Block, Borders, List, Row, Table, Text};
use tui::Frame;

pub fn draw_source_tab<B>(
    f: &mut Frame<B>,
    app: &mut App,
    area: Rect,
    current_focus: &[FocusLayerType],
) where
    B: Backend,
{
    // Split the tab area
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(25), Constraint::Percentage(75)].as_ref())
        .split(area);

    let lists_area = Layout::default()
        .constraints([Constraint::Percentage(30), Constraint::Percentage(70)].as_ref())
        .split(chunks[0]);

    // Remove focus: SourceTab
    // Draw focus: SourceOriginList
    let current_focus = &current_focus[1..];
    let items = app
        .root_state
        .source_origin_units
        .iter()
        .map(|(k, _)| Text::raw(k));
    let list = List::new(items)
        .block(
            Block::default()
                .title("Origin")
                .borders(Borders::ALL)
                .border_style(
                    Style::default().fg(focus_border_fg(app, FocusLayerType::SourceOriginList)),
                ),
        )
        .style(Style::default().fg(Color::White))
        .highlight_style(Style::default().fg(Color::Yellow));

    f.render_stateful_widget(
        list,
        lists_area[0],
        &mut app.root_state.source_tab.origin_selected,
    );

    // Remove focus: SourceOriginList
    // Draw focus: SourceUnitList
    let _current_focus = &current_focus[1..];
    match app.root_state.source_tab.origin_selected.selected() {
        Some(o) => {
            render_source_unit_list(f, app, lists_area[1], o);
            // TODO: Check focus and switch on expanded info or table

            // Draw focus: SourcePointTable
            match app.root_state.source_tab.unit_selected.selected() {
                Some(u) => render_source_point_table(f, app, chunks[1], o, u),
                None => {
                    // Render empty unit selection
                    f.render_widget(
                        Block::default()
                            .title("Points")
                            .borders(Borders::ALL)
                            .border_style(
                                Style::default()
                                    .fg(focus_border_fg(app, FocusLayerType::SourcePointTable)),
                            ),
                        chunks[1],
                    );
                }
            }
        }
        None => {
            // No origin selection done - render empty block
            f.render_widget(
                Block::default()
                    .title("Unit")
                    .borders(Borders::ALL)
                    .border_style(
                        Style::default().fg(focus_border_fg(app, FocusLayerType::SourceUnitList)),
                    ),
                lists_area[1],
            );
        }
    }
}

fn render_source_unit_list<B>(f: &mut Frame<B>, app: &mut App, area: Rect, origin_index: usize)
where
    B: Backend,
{
    let items = app.root_state.source_origin_units[origin_index]
        .1
        .iter()
        .map(Text::raw);
    let list = List::new(items)
        .block(
            Block::default()
                .title("Unit")
                .borders(Borders::ALL)
                .border_style(
                    Style::default().fg(focus_border_fg(app, FocusLayerType::SourceUnitList)),
                ),
        )
        .style(Style::default().fg(Color::White))
        .highlight_style(Style::default().fg(Color::Yellow));

    f.render_stateful_widget(list, area, &mut app.root_state.source_tab.unit_selected);
}

fn render_source_point_table<B>(
    f: &mut Frame<B>,
    app: &mut App,
    area: Rect,
    origin_index: usize,
    unit_index: usize,
) where
    B: Backend,
{
    let origin = &app.root_state.source_origin_units[origin_index].0;
    let unit = &app.root_state.source_origin_units[origin_index].1[unit_index];
    let key = (origin.clone(), unit.clone());

    let table = Table::new(
        ["id", "name", "description"].iter(),
        app.root_state.source_points[&key].iter().map(|p| {
            let row_style = Style::default().fg(Color::White);
            Row::StyledData(
                vec![&p.id, &p.source_name, &p.description].into_iter(),
                row_style,
            )
        }),
    )
    .block(
        Block::default()
            .title("Points in Unit")
            .borders(Borders::ALL)
            .border_style(
                Style::default().fg(focus_border_fg(app, FocusLayerType::SourcePointTable)),
            ),
    )
    .header_style(Style::default().fg(Color::Yellow))
    .widths(&[
        Constraint::Percentage(20),
        Constraint::Percentage(20),
        Constraint::Percentage(60),
    ])
    .style(Style::default().fg(Color::White))
    .column_spacing(1);

    f.render_stateful_widget(table, area, &mut app.root_state.source_tab.point_selected);
}
