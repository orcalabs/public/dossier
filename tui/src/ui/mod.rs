//! Render the UI for the terminal application.

mod generic;
mod source;

use self::{generic::draw_generic_tab, source::draw_source_tab};
use crate::app::focus::FocusLayerType;
use crate::app::App;

use std::io;
use tui::backend::Backend;
use tui::layout::{Constraint, Layout};
use tui::style::{Color, Style};
use tui::widgets::{Block, Borders, Tabs};
use tui::Terminal;

// Utility function
fn focus_border_fg(app: &App, layer: FocusLayerType) -> Color {
    let front: bool = {
        let mut iter = app.focus.iter();
        match iter.next_back() {
            Some(ref f) if f.layer_type == layer => true,
            Some(_) => false,
            None => false,
        }
    };
    if front {
        Color::Red
    } else {
        Color::White
    }
}

/// Main draw method to render the terminal ui.
pub fn draw<B: Backend>(terminal: &mut Terminal<B>, app: &mut App) -> Result<(), io::Error> {
    // Retrieve a slice of the focus stack
    let focus: Vec<FocusLayerType> = app.focus.iter().map(|f| f.layer_type).collect();
    let current_focus: &[FocusLayerType] = &focus;

    terminal.draw(|mut f| {
        // We chunk the screen into its main high level components which will always
        // be valid regardless off content on screen.
        let chunks = Layout::default()
            .constraints([Constraint::Length(3), Constraint::Min(0)].as_ref())
            .split(f.size());

        // Draw focus: Application
        // Render a top-level tabs which is sticky on the top the the screen.
        let tabs = Tabs::default()
            .block(
                Block::default()
                    .borders(Borders::ALL)
                    .border_style(
                        Style::default().fg(focus_border_fg(app, FocusLayerType::GenericTab)),
                    )
                    .title("Data Explorer"),
            )
            .titles(&app.root_state.tabs)
            .style(Style::default())
            .highlight_style(Style::default().fg(Color::Yellow))
            .divider(tui::symbols::DOT)
            .select(app.root_state.tab_index);

        f.render_widget(tabs, chunks[0]);
        // Draw focus: GenericTab
        match app.root_state.tab_index {
            0 => draw_generic_tab(&mut f, app, chunks[1], &current_focus[1..]),
            1 => draw_source_tab(&mut f, app, chunks[1], &current_focus[1..]),
            _ => {}
        }
    })
}
