//! Create and render the generic tab components.

use super::focus_border_fg;
use crate::app::breadcrumbs::DataPointBreadcrumbs;
use crate::app::focus::FocusLayerType;
use crate::app::App;

use dossier::schema::generic_point::{GenericPoint, GenericPointReference};
use dossier::schema::source_point::SourcePointReference;
use tui::backend::Backend;
use tui::layout::{Alignment, Constraint, Direction, Layout, Rect};
use tui::style::{Color, Style};
use tui::widgets::{
    Block, Borders, List, Paragraph, Row, StatefulWidget, Table, TableState, Text, Widget,
};
use tui::Frame;

pub fn draw_generic_tab<B>(
    f: &mut Frame<B>,
    app: &mut App,
    area: Rect,
    current_focus: &[FocusLayerType],
) where
    B: Backend,
{
    // Split the tab area
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(25), Constraint::Percentage(75)].as_ref())
        .split(area);

    // Remove focus: GenericTab
    // Draw focus: GenericUnitList
    let current_focus = &current_focus[1..];
    let items = app.root_state.generic_units.iter().map(Text::raw);
    let list = List::new(items)
        .block(
            Block::default()
                .title("Units")
                .borders(Borders::ALL)
                .border_style(
                    Style::default().fg(focus_border_fg(app, FocusLayerType::GenericUnitList)),
                ),
        )
        .style(Style::default().fg(Color::White))
        .highlight_style(Style::default().fg(Color::Yellow));

    f.render_stateful_widget(
        list,
        chunks[0],
        &mut app.root_state.generic_tab.unit_selected,
    );

    // Remove focus: GenericUnitList
    let current_focus = &current_focus[1..];
    // Determine draw within selection widget
    match current_focus {
        // Expanded info is last in stack
        [_, FocusLayerType::GenericPointExpandedInfo, ..] => {
            render_generic_point_expanded_info(f, app, chunks[1])
        }
        // Point table is last in stack
        [FocusLayerType::GenericPointTable] => render_generic_point_table_focus(f, app, chunks[1]),
        // Empty focus stack
        [] => render_generic_point_table_focus(f, app, chunks[1]),
        _ => todo!(),
    }
}

fn render_generic_point_table_focus<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    // Render table
    match app.root_state.generic_tab.unit_selected.selected() {
        Some(i) => match app
            .root_state
            .generic_points
            .get(&app.root_state.generic_units[i])
        {
            Some(v) => {
                let table = create_selected_generic_unit_widget(&app, v);
                f.render_stateful_widget(
                    table,
                    area,
                    &mut app.root_state.generic_tab.point_selected,
                );
            }
            None => {
                let w = create_empty_generic_unit_widget(&app);
                f.render_widget(w, area);
            }
        },
        None => {
            let w = create_empty_generic_unit_widget(&app);
            f.render_widget(w, area);
        }
    }
}

fn render_generic_point_expanded_info<B>(f: &mut Frame<B>, app: &mut App, area: Rect)
where
    B: Backend,
{
    // Create the block wrapping all content
    let block = Block::default()
        .title("Expanded Point Info")
        .borders(Borders::ALL)
        .border_style(Style::default().fg(focus_border_fg(
            app,
            // NOTE: Should check of both Source and Generic, but only the Source one is ever
            // highlighted.
            FocusLayerType::SourcePointExpandedInfo,
        )));
    let inner = block.inner(area);
    f.render_widget(block, area);

    // Split into breadcrumbs and rest
    let chunks = Layout::default()
        .constraints([Constraint::Min(2), Constraint::Min(50)].as_ref())
        .split(inner);

    // Render breadcrumb line on top
    let paragraph = Paragraph::new(app.root_state.generic_tab.breadcrumbs.texts.iter());
    f.render_widget(paragraph, chunks[0]);

    // Split the inner Rect into 3 areas:
    // * Table view
    // * Description
    // * List views
    // ** split in two, vertically
    let chunks = Layout::default()
        .constraints(
            [
                Constraint::Ratio(1, 3),
                Constraint::Ratio(1, 3),
                Constraint::Ratio(1, 3),
            ]
            .as_ref(),
        )
        .split(chunks[1]);

    match app
        .root_state
        .generic_tab
        .breadcrumbs
        .last()
        .cloned()
        .unwrap()
    {
        DataPointBreadcrumbs::Generic(g) => render_expanded_generic_crumb(f, app, chunks, g),
        DataPointBreadcrumbs::Source(s) => render_expanded_source_crumb(f, app, chunks, s),
    }
}

fn render_expanded_generic_crumb<B>(
    f: &mut Frame<B>,
    app: &mut App,
    chunks: Vec<Rect>,
    reference: GenericPointReference,
) where
    B: Backend,
{
    let point = &app.store.generic(&reference);

    // Calculate and render short comprehensive info in table about point

    f.render_widget(
        Table::new(
            ["", ""].iter(),
            vec![
                Row::Data(["unit:", &point.unit].iter()),
                Row::Data(["id:", &point.id].iter()),
                Row::Data(["display:", &point.display_name].iter()),
                Row::Data(["code:", &point.code_name].iter()),
                Row::Data(["field type:", &point.field_type].iter()),
            ]
            .into_iter(),
        )
        .widths(&[Constraint::Length(14), Constraint::Length(80)])
        .style(Style::default().fg(Color::White))
        .column_spacing(1),
        chunks[0],
    );

    // Render description
    f.render_widget(
        Paragraph::new([Text::raw(&point.description)].iter())
            .style(Style::default().fg(Color::White))
            .wrap(true),
        chunks[1],
    );

    // Split area for Composition and SourcePoints
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
        .split(chunks[2]);

    // Render Composition list
    f.render_stateful_widget(
        List::new(
            point
                .composition
                .as_ref()
                .cloned()
                .unwrap_or_else(Vec::new)
                .iter()
                .map(|p| Text::raw(format!("{}:{}", p.unit, p.id))),
        )
        .block(
            Block::default()
                .title("Composition")
                .borders(Borders::TOP | Borders::RIGHT)
                .border_style(Style::default().fg(focus_border_fg(
                    app,
                    FocusLayerType::GenericPointCompositionList,
                ))),
        )
        .style(Style::default().fg(Color::White))
        .highlight_style(Style::default().fg(Color::Yellow)),
        chunks[0],
        &mut app.root_state.generic_tab.composition_selected,
    );

    // Render Source Points
    let map_key = GenericPointReference {
        unit: point.unit.clone(),
        id: point.id.clone(),
    };
    f.render_stateful_widget(
        List::new(
            app.store
                // NOTE: This is a owned result, not borrowed.
                .source_points_from_generic_reference(&map_key)
                .unwrap_or_else(Vec::new)
                .iter()
                .map(|p| Text::raw(format!("{}:{}", p.unit, p.id))),
        )
        .block(
            Block::default()
                .title("Source Points")
                .borders(Borders::TOP | Borders::LEFT)
                .border_style(
                    Style::default()
                        .fg(focus_border_fg(app, FocusLayerType::GenericPointSourceList)),
                ),
        )
        .style(Style::default().fg(Color::White))
        .highlight_style(Style::default().fg(Color::Yellow)),
        chunks[1],
        &mut app.root_state.generic_tab.source_selected,
    );
}

fn render_expanded_source_crumb<B>(
    f: &mut Frame<B>,
    app: &mut App,
    chunks: Vec<Rect>,
    reference: SourcePointReference,
) where
    B: Backend,
{
    let point = &app.store.source(&reference);

    f.render_widget(
        Table::new(
            ["", ""].iter(),
            vec![
                Row::Data(["unit:", &point.unit].iter()),
                Row::Data(["id:", &point.id].iter()),
                Row::Data(["source name:", &point.source_name].iter()),
                Row::Data(["field type:", &point.field_type].iter()),
            ]
            .into_iter(),
        )
        .widths(&[Constraint::Length(14), Constraint::Length(80)])
        .style(Style::default().fg(Color::White))
        .column_spacing(1),
        chunks[0],
    );

    // Render description
    f.render_widget(
        Paragraph::new([Text::raw(&point.description)].iter())
            .style(Style::default().fg(Color::White))
            .wrap(true),
        chunks[1],
    );
}

// Allow ptr_arg due to lifetime issues
#[allow(clippy::ptr_arg)]
fn create_selected_generic_unit_widget(
    app: &App,
    points: &Vec<GenericPoint>,
) -> impl StatefulWidget<State = TableState> {
    Table::new(
        ["id", "name", "description", "composition"].iter(),
        points.clone().into_iter().map(|p| {
            let row_style = Style::default().fg(Color::White);
            let mut comps = String::new();
            if let Some(composition) = p.composition {
                comps.extend(format!("[{}] ", composition.len()).chars());
                for c in composition.iter() {
                    comps.extend(format!("{}:{} ", c.unit, c.id).chars())
                }
            } else {
                comps.push_str(&"[0]".to_string());
            }
            Row::StyledData(
                vec![p.id, p.display_name, p.description, comps].into_iter(),
                row_style,
            )
        }),
    )
    .block(
        Block::default()
            .title("Points in Unit")
            .borders(Borders::ALL)
            .border_style(
                Style::default().fg(focus_border_fg(app, FocusLayerType::GenericPointTable)),
            ),
    )
    .header_style(Style::default().fg(Color::DarkGray))
    .widths(&[
        Constraint::Percentage(10),
        Constraint::Percentage(20),
        Constraint::Percentage(50),
        Constraint::Percentage(20),
    ])
    .style(Style::default().fg(Color::White))
    .highlight_style(Style::default().fg(Color::Yellow))
    .column_spacing(1)
}

fn create_empty_generic_unit_widget(app: &App) -> impl Widget + '_ {
    Paragraph::new(app.root_state.generic_tab.points_placeholder_text.iter())
        .block(
            Block::default()
                .title("Generic Points - not loaded")
                .borders(Borders::ALL),
        )
        .style(Style::default().fg(Color::White))
        .alignment(Alignment::Center)
        .wrap(true)
}
